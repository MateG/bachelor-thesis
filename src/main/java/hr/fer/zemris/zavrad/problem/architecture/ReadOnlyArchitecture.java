package hr.fer.zemris.zavrad.problem.architecture;

/**
 * Represents a read-only FPGA specification.
 *
 * @author Mate Gašparini
 */
public class ReadOnlyArchitecture implements Architecture {

    /** Number of rows of configurable logic blocks (CLBs). */
    private final int clbRows;

    /** Number of columns of configurable logic blocks (CLBs). */
    private final int clbColumns;

    /** Number of inputs on a single configurable logic block (CLB). */
    private final int clbInputs;

    /** Number of conducting wires on each wire segment. */
    private final int conductorsPerSegment;

    /** Number of I/O pins crossing each wire segment. */
    private final int pinsPerSegment;

    /**
     * Constructor specifying the FPGA architecture.
     *
     * @param clbRows Number of rows of CLBs.
     * @param clbColumns Number of columns of CLBs.
     * @param clbInputs Number of inputs on a single CLB.
     * @param conductorsPerSegment Number of conductors on each wire segment.
     * @param pinsPerSegment Number of I/O pins crossing each wire segment.
     * @throws IllegalArgumentException If illogical values are specified.
     */
    public ReadOnlyArchitecture(int clbRows, int clbColumns, int clbInputs,
            int conductorsPerSegment, int pinsPerSegment) {
        this.clbRows = check(clbRows, 1, "CLB row");
        this.clbColumns = check(clbColumns, 1, "CLB column");
        this.clbInputs = check(clbInputs, 2, "CLB input");
        this.conductorsPerSegment = check(
                conductorsPerSegment, 1, "Conductors per segment");
        this.pinsPerSegment = check(pinsPerSegment, 1, "Pins per segment");
    }

    @Override
    public int getClbRows() {
        return clbRows;
    }

    @Override
    public int getClbColumns() {
        return clbColumns;
    }

    @Override
    public int getClbInputs() {
        return clbInputs;
    }

    @Override
    public int getConductorsPerSegment() {
        return conductorsPerSegment;
    }

    @Override
    public int getPinsPerSegment() {
        return pinsPerSegment;
    }

    /**
     * Checks if the given value is at least as high as the given minimum value.
     *
     * @param value The given value.
     * @param min The given minimum value.
     * @param name The name of the attribute used for the exception message.
     * @return The given value.
     * @throws IllegalArgumentException If the given value is lower than the
     *         given minimum value.
     */
    private int check(int value, int min, String name) {
        if (value < min) {
            throw new IllegalArgumentException(
                    name + " must be at least " + min + ", but was " + value
            );
        }
        return value;
    }
}
