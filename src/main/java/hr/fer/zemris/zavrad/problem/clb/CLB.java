package hr.fer.zemris.zavrad.problem.clb;

import java.util.Arrays;
import java.util.Objects;

/**
 * Represents read-only information about a single configurable logic block.
 *
 * @author Mate Gašparini
 */
public class CLB {

    /** The CLB identifier. */
    private final String id;

    /** The CLB input identifiers. */
    private final String[] inputs;

    /** Optional label of a function which the CLB realizes. */
    private final String functionLabel;

    /**
     * Constructor specifying the configurable logic block.
     *
     * @param id The specified CLB identifier.
     * @param inputs The specified CLB input identifiers.
     * @param functionLabel The function which the CLB realizes (optional).
     */
    public CLB(String id, String[] inputs, String functionLabel) {
        this.id = Objects.requireNonNull(id, "CLB id must not be null");
        this.functionLabel = functionLabel;
        Objects.requireNonNull(inputs, "CLB inputs must not be null");
        this.inputs = new String[inputs.length];
        for (int i = 0; i < inputs.length; i ++) {
            this.inputs[i] = Objects.requireNonNull(inputs[i],
                    "CLB inputs must not be null");
        }
    }

    /**
     * Returns the configurable logic block identifier.
     *
     * @return The CLB identifier.
     */
    public String getId() {
        return id;
    }

    /**
     * Returns the configurable logic block input identifiers.
     *
     * @return The CLB input identifiers.
     */
    public String[] getInputs() {
        return inputs;
    }

    /**
     * Returns the function label which the CLB realizes, or {@code null} if the
     * CLB does not realize a labeled function.
     *
     * @return The realized function label.
     */
    public String getFunctionLabel() {
        return functionLabel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CLB clb = (CLB) o;
        return id.equals(clb.id) &&
                Arrays.equals(inputs, clb.inputs) &&
                Objects.equals(functionLabel, clb.functionLabel);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(id, functionLabel);
        result = 31 * result + Arrays.hashCode(inputs);
        return result;
    }

    @Override
    public String toString() {
        return "CLB{" +
                "id='" + id + '\'' +
                ", inputs=" + Arrays.toString(inputs) +
                ", functionLabel='" + functionLabel + '\'' +
                '}';
    }
}
