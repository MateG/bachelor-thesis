package hr.fer.zemris.zavrad.problem.clb;

/**
 * Thrown to indicate that the {@link CLBParser} has failed to parse the given
 * input.
 *
 * @author Mate Gašparini
 */
public class CLBParserException extends RuntimeException {

    /**
     * Constructs an {@code CLBParserException} with no detail message.
     */
    public CLBParserException() {
    }

    /**
     * Constructs an {@code CLBParserException} with the specified detail
     * message.
     *
     * @param message The specified detail message.
     */
    public CLBParserException(String message) {
        super(message);
    }

    /**
     * Constructs an {@code CLBParserException} with the specified detail
     * message and cause.
     *
     * @param message The specified detail message.
     * @param cause The specified cause.
     */
    public CLBParserException(String message, Throwable cause) {
        super(message, cause);
    }
}
