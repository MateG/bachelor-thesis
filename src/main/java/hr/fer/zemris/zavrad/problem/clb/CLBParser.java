package hr.fer.zemris.zavrad.problem.clb;

import hr.fer.zemris.zavrad.problem.architecture.Architecture;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

/**
 * Represents a parser responsible for translating some textual input (e.g. from
 * a file) to a {@code List} of {@link CLB}s and {@code Map} of I/O pin labels
 * (optional).
 *
 * @author Mate Gašparini
 */
public class CLBParser {

    /** Marks the start of lines which should be ignored. */
    private static final String COMMENT = "#";

    /** Marks an assignment operation. */
    private static final String ASSIGNMENT = ":";

    /** The specified FPGA architecture. */
    private Architecture architecture;

    /** The resulting parsed configurable logic blocks. */
    private List<CLB> blocks;

    /** The resulting parsed {@code Map} of I/O pin labels. */
    private Map<String, String> pinMap;

    /** Helper variable used to determine the current line of input. */
    private int lineIndex;

    /**
     * Constructs the parser and tries to parse the specified input.
     *
     * @param lines The lines of the specified input.
     * @param architecture The specified FPGA architecture.
     * @throws CLBParserException If the specified input is not valid.
     */
    public CLBParser(List<String> lines, Architecture architecture) {
        this.architecture = Objects.requireNonNull(architecture,
                "Architecture must not be null");
        parse(lines);
    }

    /**
     * Constructs the parser and tries to parse the specified input.
     *
     * @param data The specified input.
     * @param architecture The specified FPGA architecture.
     * @throws CLBParserException If the specified input is not valid.
     */
    public CLBParser(String data, Architecture architecture) {
        this(Arrays.asList(data.split("\n")), architecture);
    }

    /**
     * Constructs the parser and tries to parse the specified input.
     *
     * @param path Path to the file with the specified input.
     * @param architecture The specified FPGA architecture.
     * @throws IOException If an IO error occurs.
     * @throws CLBParserException If the specified input is not valid.
     */
    public CLBParser(Path path, Architecture architecture) throws IOException {
        this(Files.readAllLines(path), architecture);
    }

    /**
     * Returns the {@code List} of configurable logic blocks parsed from the
     * specified input during the construction.
     *
     * @return The parsed {@link CLB}s.
     */
    public List<CLB> getBlocks() {
        return blocks;
    }

    /**
     * Returns the {@code Map} of input/output labels and beforehand assigned
     * I/O pins parsed from the specified input during the construction.
     *
     * @return The parsed {@code Map} of I/O pin labels.
     */
    public Map<String, String> getPinMap() {
        return pinMap;
    }

    /**
     * Parses the given lines of input and stores the parsed information.
     *
     * @param lines The given input.
     * @throws CLBParserException If the given input is not valid.
     */
    private void parse(List<String> lines) {
        parseCLBs(lines);
        parseIOPins(lines);
    }

    /**
     * Parses the given lines of input (until either the end or a double empty
     * line is encountered) and stores the parsed CLBs.
     *
     * @param lines The given input.
     * @throws CLBParserException If the given input is not valid.
     */
    private void parseCLBs(List<String> lines) {
        blocks = new ArrayList<>();
        String id = null;
        String[] inputs = new String[architecture.getClbInputs()];
        int inputCount = 0;
        String functionLabel = null;

        boolean previousEmpty = false;
        for (; lineIndex < lines.size(); lineIndex ++) {
            String line = lines.get(lineIndex);
            if (line.startsWith(COMMENT)) continue;
            line = line.trim();

            if (line.isEmpty()) {
                if (previousEmpty) {
                    break; // All CLB definitions have been read.
                } else {
                    checkInputCount(inputCount, inputs.length);
                    id = null;
                    inputCount = 0;
                    functionLabel = null;
                    previousEmpty = true;
                    continue;
                }
            }
            previousEmpty = false;

            if (id == null) {
                String[] parts = line.split(ASSIGNMENT);
                if (parts.length == 1) {
                    id = line;
                } else if (parts.length == 2 && !parts[1].isEmpty()) {
                    id = parts[0];
                    functionLabel = parts[1];
                } else {
                    throw new CLBParserException("Invalid assignment: " + line);
                }
                if (!id.startsWith("CLB(") || !id.endsWith(")")) {
                    throw new CLBParserException("CLB identifier must be of format CLB(n)");
                }
                continue;
            }

            checkInputCountOver(inputCount, inputs.length);
            inputs[inputCount++] = line;
            if (inputCount == inputs.length) {
                blocks.add(new CLB(id, inputs, functionLabel));
            }
        }

        checkBlockCount();
    }

    /**
     * Parses the remaining lines of input and stores the parsed I/O pin labels.
     *
     * @param lines The given input.
     * @throws CLBParserException If the given input is not valid.
     */
    private void parseIOPins(List<String> lines) {
        pinMap = new HashMap<>();
        for (; lineIndex < lines.size(); lineIndex ++) {
            String line = lines.get(lineIndex);
            if (line.startsWith(COMMENT)) continue;
            line = line.trim();
            if (line.isEmpty()) continue;

            String[] parts = line.split(ASSIGNMENT);
            if (parts.length != 2 || parts[1].isEmpty()) {
                throw new CLBParserException("Invalid pin assignment: " + line);
            }

            pinMap.put(parts[0], parts[1]);
        }
    }

    /**
     * Checks the validity of the CLB count (determined by the specified FPGA
     * architecture).
     *
     * @throws CLBParserException If the CLB count is not valid.
     */
    private void checkBlockCount() {
        int size = blocks.size();
        if (size == 0) {
            throw new CLBParserException("No CLB definitions found");
        }

        int max = architecture.getClbRows()*architecture.getClbColumns();
        if (size > max) {
            throw new CLBParserException(
                    "Expected at most " + max + " CLB definitions, got " + size
            );
        }
    }

    /**
     * Checks if the given input count is the same as the expected value.
     *
     * @param inputCount The given input count.
     * @param expected The expected value.
     * @throws CLBParserException If the given input count is not equal to the
     *         expected value.
     */
    private void checkInputCount(int inputCount, int expected) {
        if (inputCount != expected) {
            throwInvalidInputCount(inputCount, expected);
        }
    }

    /**
     * Checks if the given input count is greater or equal than the expected
     * value.
     *
     * @param inputCount The given input count.
     * @param expected The expected value.
     * @throws CLBParserException If the given input count is greater or equal
     *         than the expected value.
     */
    private void checkInputCountOver(int inputCount, int expected) {
        if (inputCount >= expected) {
            throwInvalidInputCount(inputCount+1, expected);
        }
    }

    /**
     * Throws the {@link CLBParserException} with a specific message.
     *
     * @param inputCount The specified actual input count value.
     * @param expected The expected input count value.
     * @throws CLBParserException Always.
     */
    private void throwInvalidInputCount(int inputCount, int expected) {
        throw new CLBParserException(
                "Expected " + expected + " CLB inputs, got " + inputCount
        );
    }
}
