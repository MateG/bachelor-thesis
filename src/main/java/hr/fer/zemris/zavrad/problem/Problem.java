package hr.fer.zemris.zavrad.problem;

import hr.fer.zemris.zavrad.problem.architecture.Architecture;
import hr.fer.zemris.zavrad.problem.clb.CLB;

import java.util.List;
import java.util.Map;

/**
 * Models an FPGA optimization problem with a specific {@link Architecture},
 * configurable logic blocks (CLBs) and pin mappings (optional).
 *
 * @author Mate Gašparini
 */
public class Problem implements Architecture {

    /** The specified FPGA architecture. */
    private Architecture architecture;

    /** The specified configurable logic blocks. */
    private List<CLB> blocks;

    /** The specified I/O pin mappings. */
    private Map<String, String> pinMap;

    /**
     * Constructor specifying the problem.
     *
     * @param architecture The specified FPGA architecture.
     * @param blocks The specified configurable logic blocks.
     * @param pinMap The specified I/O pin mappings.
     */
    public Problem(Architecture architecture, List<CLB> blocks,
            Map<String, String> pinMap) {
        this.architecture = architecture;
        this.blocks = blocks;
        this.pinMap = pinMap;
    }

    @Override
    public int getClbRows() {
        return architecture.getClbRows();
    }

    @Override
    public int getClbColumns() {
        return architecture.getClbColumns();
    }

    @Override
    public int getClbInputs() {
        return architecture.getClbInputs();
    }

    @Override
    public int getConductorsPerSegment() {
        return architecture.getConductorsPerSegment();
    }

    @Override
    public int getPinsPerSegment() {
        return architecture.getPinsPerSegment();
    }

    /**
     * Returns the specified {@code List} of configurable logic blocks (CLBs).
     *
     * @return The specified CLBs.
     */
    public List<CLB> getBlocks() {
        return blocks;
    }

    /**
     * Returns the {@code Map} of input/output labels and I/O pins assigned to
     * them.
     *
     * @return The specified I/O pin mappings.
     */
    public Map<String, String> getPinMap() {
        return pinMap;
    }
}
