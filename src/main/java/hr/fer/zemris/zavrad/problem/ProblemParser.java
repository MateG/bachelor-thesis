package hr.fer.zemris.zavrad.problem;

import hr.fer.zemris.zavrad.problem.architecture.Architecture;
import hr.fer.zemris.zavrad.problem.architecture.ArchitectureParser;
import hr.fer.zemris.zavrad.problem.clb.CLBParser;

import java.io.IOException;
import java.nio.file.Path;

/**
 * Represents a parser pair ({@link ArchitectureParser} and {@link CLBParser})
 * responsible for defining an FPGA optimization {@link Problem}.
 *
 * @author Mate Gašparini
 */
public class ProblemParser {

    /** The parsed FPGA optimization problem. */
    private Problem problem;

    /**
     * Constructs the parsers and tries to parse the specified inputs.
     *
     * @param architecturePath Path to the file with the specified architecture.
     * @param clbPath Path to the file with the specified CLBs.
     * @throws IOException If an IO error occurs.
     */
    public ProblemParser(Path architecturePath, Path clbPath)
            throws IOException {
        Architecture architecture = new ArchitectureParser(architecturePath)
                .getArchitecture();
        CLBParser clbParser = new CLBParser(clbPath, architecture);
        problem = new Problem(
                architecture,
                clbParser.getBlocks(),
                clbParser.getPinMap()
        );
    }

    /**
     * Returns the parsed FPGA optimization problem.
     *
     * @return The defined problem.
     */
    public Problem getProblem() {
        return problem;
    }
}
