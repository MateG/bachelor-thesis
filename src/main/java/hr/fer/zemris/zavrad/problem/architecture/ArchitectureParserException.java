package hr.fer.zemris.zavrad.problem.architecture;

/**
 * Thrown to indicate that the {@link ArchitectureParser} has failed to parse
 * the given input.
 *
 * @author Mate Gašparini
 */
public class ArchitectureParserException extends RuntimeException {

    /**
     * Constructs an {@code ArchitectureParserException} with no detail message.
     */
    public ArchitectureParserException() {
    }

    /**
     * Constructs an {@code ArchitectureParserException} with the specified
     * detail message.
     *
     * @param message The specified detail message.
     */
    public ArchitectureParserException(String message) {
        super(message);
    }

    /**
     * Constructs an {@code ArchitectureParserException} with the specified
     * detail message and cause.
     *
     * @param message The specified detail message.
     * @param cause The specified cause.
     */
    public ArchitectureParserException(String message, Throwable cause) {
        super(message, cause);
    }
}
