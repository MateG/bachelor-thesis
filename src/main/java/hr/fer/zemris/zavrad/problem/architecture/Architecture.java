package hr.fer.zemris.zavrad.problem.architecture;

/**
 * Models some FPGA architecture specification.
 *
 * @author Mate Gašparini
 */
public interface Architecture {

    /**
     * Returns the number of rows of configurable logic blocks (CLBs).
     *
     * @return Number of rows of CLBs.
     */
    int getClbRows();

    /**
     * Returns the number of columns of configurable logic blocks (CLBs).
     *
     * @return Number of columns of CLBs.
     */
    int getClbColumns();

    /**
     * Returns the number of inputs on a single configurable logic block (CLB).
     *
     * @return Number of inputs on a single CLB.
     */
    int getClbInputs();

    /**
     * Returns the number of conducting wires on each wire segment.
     *
     * @return Number of conductors on each wire segment.
     */
    int getConductorsPerSegment();

    /**
     * Returns the number of I/O pins crossing each wire segment.
     *
     * @return Number of I/O pins crossing each wire segment.
     */
    int getPinsPerSegment();
}
