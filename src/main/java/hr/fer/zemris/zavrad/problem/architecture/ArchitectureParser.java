package hr.fer.zemris.zavrad.problem.architecture;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;

/**
 * Represents a parser responsible for translating some textual input (e.g. from
 * a file) to an {@link Architecture} object.
 *
 * @author Mate Gašparini
 */
public class ArchitectureParser {

    /** Expected number of parameters which define the FPGA architecture. */
    private static final int PARAMETER_COUNT = 5;

    /** Marks the start of lines which should be ignored. */
    private static final String COMMENT = "#";

    /** The resulting parsed FPGA architecture. */
    private Architecture architecture;

    /**
     * Constructs the parser and tries to parse the specified input to a valid
     * FPGA architecture.
     *
     * @param lines The lines of the specified input.
     * @throws ArchitectureParserException If the specified input is not valid.
     */
    public ArchitectureParser(List<String> lines) {
        parse(lines);
    }

    /**
     * Constructs the parser and tries to parse the specified input to a valid
     * FPGA architecture.
     *
     * @param data The specified input.
     * @throws ArchitectureParserException If the specified input is not valid.
     */
    public ArchitectureParser(String data) {
        this(Arrays.asList(data.split("\n")));
    }

    /**
     * Constructs the parser and tries to parse the specified input to a valid
     * FPGA architecture.
     *
     * @param path Path to the file with the specified input.
     * @throws ArchitectureParserException If the specified input is not valid.
     */
    public ArchitectureParser(Path path) throws IOException {
        this(Files.readAllLines(path));
    }

    /**
     * Returns the FPGA architecture parsed from the specified input during the
     * construction.
     *
     * @return The parsed FPGA architecture.
     */
    public Architecture getArchitecture() {
        return architecture;
    }

    /**
     * Parses the given lines of input and stores the parsed FPGA architecture.
     *
     * @param lines The given input.
     * @throws ArchitectureParserException If the given input is not valid.
     */
    private void parse(List<String> lines) {
        int[] values = new int[PARAMETER_COUNT];
        int i = 0;
        for (String line : lines) {
            if (line.startsWith(COMMENT)) continue;
            line = line.trim();
            if (line.isEmpty()) continue;

            try {
                if (i >= PARAMETER_COUNT) {
                    throw new ArchitectureParserException(
                            "Too many architecture parameters"
                    );
                }
                values[i++] = Integer.parseInt(line);
            } catch (NumberFormatException ex) {
                throw new ArchitectureParserException(
                        "Invalid architecture parameter: " + line
                );
            }
        }

        if (i != PARAMETER_COUNT) {
            throw new ArchitectureParserException(
                    "Expected " + PARAMETER_COUNT + " parameters, got " + i
            );
        }

        architecture = new ReadOnlyArchitecture(
                values[0], values[1], values[2], values[3], values[4]
        );
    }
}
