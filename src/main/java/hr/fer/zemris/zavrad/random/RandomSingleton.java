package hr.fer.zemris.zavrad.random;

import java.util.Random;

public class RandomSingleton {

    private static Random instance = new Random();

    public static Random getInstance() {
        return instance;
    }
}
