package hr.fer.zemris.zavrad;

import hr.fer.zemris.zavrad.algorithm.OptimizationAlgorithm;
import hr.fer.zemris.zavrad.algorithm.solution.Solution;
import hr.fer.zemris.zavrad.algorithm.solution.SolutionPrinter;
import hr.fer.zemris.zavrad.problem.Problem;
import hr.fer.zemris.zavrad.problem.ProblemParser;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FPGAOptimizer {

    private static final String SOLUTION_FILENAME = "best-solution.txt";

    public static void main(String[] args) throws IOException {
        try {
            Problem problem = getProblem(args);
            OptimizationAlgorithm algorithm = new OptimizationAlgorithm(problem);
            Solution solution = algorithm.solve();
            if (solution == null) return;

            SolutionPrinter saver = new SolutionPrinter(solution, problem);
            System.out.print("Writing best solution to "
                    + SOLUTION_FILENAME + "... ");
            saver.writeToFile(Paths.get(SOLUTION_FILENAME));
            System.out.println("Done");
        } catch (RuntimeException ex) {
            System.err.println(ex.getMessage());
        }
    }

    private static Problem getProblem(String[] args) {
        if (args.length != 2) {
            System.err.println("Expected 2 arguments - architecture and clb definition paths.");
            System.exit(1);
        }

        Path architecturePath = null;
        try {
            architecturePath = Paths.get(args[0]);
            if (!Files.exists(architecturePath)) {
                System.err.println(args[0] + " does not exist.");
                System.exit(1);
            } else if (!Files.isReadable(architecturePath)) {
                System.err.println(args[0] + " is not readable.");
                System.exit(1);
            }
        } catch (InvalidPathException ex) {
            System.err.println(args[0] + " is not a valid file path.");
            System.exit(1);
        }

        Path clbPath = null;
        try {
            clbPath = Paths.get(args[1]);
            if (!Files.exists(clbPath)) {
                System.err.println(args[1] + " does not exist.");
                System.exit(1);
            } else if (!Files.isReadable(clbPath)) {
                System.err.println(args[1] + " is not readable.");
                System.exit(1);
            }
        } catch (InvalidPathException ex) {
            System.err.println(args[1] + " is not a valid file path.");
            System.exit(1);
        }

        Problem problem = null;
        try {
            problem = new ProblemParser(architecturePath, clbPath).getProblem();
        } catch (IOException ex) {
            System.err.println("An IO error occurred while trying to read the definition files.");
            System.exit(1);
        } catch (RuntimeException ex) {
            System.err.println(ex.getMessage());
            System.exit(1);
        }
        return problem;
    }
}
