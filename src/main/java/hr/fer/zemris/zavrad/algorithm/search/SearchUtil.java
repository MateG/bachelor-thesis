package hr.fer.zemris.zavrad.algorithm.search;

import hr.fer.zemris.zavrad.algorithm.fpga.Segment;
import hr.fer.zemris.zavrad.algorithm.fpga.SwitchBox;
import hr.fer.zemris.zavrad.algorithm.fpga.VerticalSegment;

import java.util.ArrayList;
import java.util.List;

public final class SearchUtil {

    /**
     * Default private constructor.
     */
    private SearchUtil() {
    }

    public static List<Segment> successors(Segment segment) {
        List<Segment> successors = new ArrayList<>();
        SwitchBox from = segment.getFrom();
        SwitchBox to = segment.getTo();

        if (segment instanceof VerticalSegment) {
            addNonNull(successors, from.getLeft());
            addNonNull(successors, from.getAbove());
            addNonNull(successors, from.getRight());
            addNonNull(successors, to.getLeft());
            addNonNull(successors, to.getBelow());
            addNonNull(successors, to.getRight());
        } else {
            addNonNull(successors, from.getBelow());
            addNonNull(successors, from.getLeft());
            addNonNull(successors, from.getAbove());
            addNonNull(successors, to.getAbove());
            addNonNull(successors, to.getRight());
            addNonNull(successors, to.getBelow());
        }
        return successors;
    }

    private static void addNonNull(List<Segment> segments, Segment segment) {
        if (segment == null) return;
        segments.add(segment);
    }
}
