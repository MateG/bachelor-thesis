package hr.fer.zemris.zavrad.algorithm.solution;

import hr.fer.zemris.zavrad.algorithm.fpga.FPGA;
import hr.fer.zemris.zavrad.algorithm.fpga.Pin;
import hr.fer.zemris.zavrad.algorithm.fpga.Segment;
import hr.fer.zemris.zavrad.algorithm.fpga.SwitchBox;
import hr.fer.zemris.zavrad.algorithm.translator.SignalTranslator;

import static hr.fer.zemris.zavrad.algorithm.solution.SwitchBoxTranslator.*;
import static hr.fer.zemris.zavrad.algorithm.translator.SignalTranslator.NO_SIGNAL;

public class SolutionCleaner {

    private Solution solution;

    private FPGA fpga;

    private int conductors;

    public SolutionCleaner(Solution solution, FPGA fpga, int conductors) {
        this.solution = solution;
        this.fpga = fpga;
        this.conductors = conductors;
    }

    public void clean() {
        cleanUnusedSwitchBoxes(fpga.getSwitchBoxes());
        cleanPins(fpga.getPins());
    }

    private void cleanUnusedSwitchBoxes(SwitchBox[][] boxes) {
        for (SwitchBox[] row : boxes) {
            for (SwitchBox box : row) {
                byte[] m = box.getMatrix();
                cleanSwitchBoxSegment(m, box.getAbove(), UPPER_DISCONNECTS);
                cleanSwitchBoxSegment(m, box.getRight(), RIGHT_DISCONNECTS);
                cleanSwitchBoxSegment(m, box.getBelow(), LOWER_DISCONNECTS);
                cleanSwitchBoxSegment(m, box.getLeft(), LEFT_DISCONNECTS);
            }
        }
    }

    private void cleanSwitchBoxSegment(byte[] matrix, Segment segment,
            byte[] lookup) {
        if (segment == null) return;
        boolean reversed = lookup == LOWER_DISCONNECTS
                || lookup == UPPER_DISCONNECTS;
        for (int i = 0; i < conductors; i ++) {
            if (matrix[i] == NO_LINKS) continue;
            int real = reversed ? conductors - 1 - i : i;
            if (segment.getConductor(real).getSignal() == NO_SIGNAL) {
                matrix[i] = lookup[matrix[i]];
            }
        }
    }

    private void cleanPins(Pin[] pins) {
        short[] IDs = solution.getIoPinIDs();
        byte[] crossings = solution.getIoPinCrossings();
        SignalTranslator translator = SignalTranslator.getInstance();
        for (int i = 0; i < pins.length; i ++) {
            Pin pin = pins[i];
            short signal = pin.getSignal();
            if (signal == NO_SIGNAL) {
                crossings[i] = NO_SIGNAL;
                continue;
            }

            Short indexConstraint = translator.getPinIndexConstraint(signal);
            if (indexConstraint != null && indexConstraint != i) {
                IDs[i] = NO_SIGNAL;
                crossings[i] = NO_SIGNAL;
                continue;
            }

            if (!translator.isInputSignal(signal)
                    && !translator.isFunctionSignal(signal)) {
                IDs[i] = NO_SIGNAL;
                crossings[i] = NO_SIGNAL;
            }
        }
    }
}
