package hr.fer.zemris.zavrad.algorithm.main;

import hr.fer.zemris.zavrad.random.RandomSingleton;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class MainSelection {

    private Random random = RandomSingleton.getInstance();

    private int n;

    private List<MainSolution> subset = new ArrayList<>();

    public MainSelection(int n) {
        this.n = n;
    }

    public MainSolution select(List<MainSolution> population) {
        for (int i = 0; i < n; i ++) {
            MainSolution randomSolution;
            do {
                int index = random.nextInt(population.size());
                randomSolution = population.get(index);
            } while (subset.contains(randomSolution));
            subset.add(population.get(random.nextInt(population.size())));
        }

        Collections.sort(subset);
        MainSolution best = subset.get(0);
        subset.clear();

        return best;
    }
}
