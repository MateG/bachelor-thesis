package hr.fer.zemris.zavrad.algorithm.solution;

import hr.fer.zemris.zavrad.algorithm.initial.StartingPointSolution;
import hr.fer.zemris.zavrad.algorithm.translator.SignalTranslator;
import hr.fer.zemris.zavrad.problem.Problem;
import hr.fer.zemris.zavrad.random.RandomSingleton;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static hr.fer.zemris.zavrad.algorithm.translator.SignalTranslator.NO_SIGNAL;

public class SolutionGenerator {

    private Random random = RandomSingleton.getInstance();

    private int clbCount;

    private int usedClbCount;

    private int boxCount;

    private int pinCount;

    private int clbInputs;

    private int inputSignalCount;

    private int conductors;

    private SignalTranslator translator;

    public SolutionGenerator(Problem problem) {
        clbCount = problem.getClbRows()*problem.getClbColumns();
        boxCount = (problem.getClbRows() + 1) * (problem.getClbColumns() + 1);
        usedClbCount = problem.getBlocks().size();
        pinCount = 2 * (problem.getClbRows() + problem.getClbColumns())
                * problem.getPinsPerSegment();
        clbInputs = problem.getClbInputs();
        conductors = problem.getConductorsPerSegment();
        translator = SignalTranslator.getInstance();
        inputSignalCount = translator.getInputSignalCount();
    }

    public Solution emptySolution() {
        return new Solution(
                new short[clbCount],
                new short[pinCount],
                new byte[pinCount],
                new byte[usedClbCount][clbInputs],
                new byte[usedClbCount],
                new byte[boxCount][conductors]
        );
    }

    public Solution randomSolution() {
        short[] ioPinIDs = randomIoPinIDs();
        return new Solution(
                randomClbIDs(),
                ioPinIDs,
                randomIoPinCrossings(ioPinIDs),
                randomClbInputCrossings(),
                randomClbOutputCrossings(),
                randomSwitchBoxes()
        );
    }

    public Solution fromStartingPoint(StartingPointSolution starting) {
        short[] original = starting.getIoPinIDs();
        short[] ioPinIDs = new short[original.length];
        System.arraycopy(original, 0, ioPinIDs, 0, ioPinIDs.length);
        for (int i = 0; i < ioPinIDs.length; i ++) {
            short signal = ioPinIDs[i];
            if (signal == NO_SIGNAL) continue;
            if (translator.isFunctionSignal(signal)) ioPinIDs[i] = NO_SIGNAL;
        }
        return new Solution(
                starting.getClbIDs(),
                ioPinIDs,
                randomIoPinCrossings(starting.getIoPinIDs()),
                randomClbInputCrossings(),
                randomClbOutputCrossings(),
                randomSwitchBoxes()
        );
    }

    private short[] randomClbIDs() {
        return randomIDs(clbCount, usedClbCount);
    }

    private short[] randomIoPinIDs() {
        if (translator.noPinConstraints()) {
            return randomIDs(pinCount, inputSignalCount);
        }

        short[] ioPinIDs = new short[pinCount];
        List<Short> freePins = new ArrayList<>(pinCount);
        for (short pin = 0; pin < pinCount; pin ++) {
            freePins.add(pin);
        }
        List<Short> freeSignals = new ArrayList<>(inputSignalCount);
        for (short signal = 1; signal <= inputSignalCount; signal ++) {
            Short pinConstraint = translator.getPinIndexConstraint(signal);
            if (pinConstraint == null) {
                freeSignals.add(signal);
            } else {
                ioPinIDs[pinConstraint] = signal;
                freePins.remove(pinConstraint);
            }
        }
        while (!freeSignals.isEmpty()) {
            int signalIndex = random.nextInt(freeSignals.size());
            short signal = freeSignals.get(signalIndex);
            freeSignals.remove(signalIndex);
            int pinIndex = random.nextInt(freePins.size());
            short pin = freePins.get(pinIndex);
            freePins.remove(pinIndex);
            ioPinIDs[pin] = signal;
        }
        return ioPinIDs;
    }

    private byte[] randomIoPinCrossings(short[] ioPinIDs) {
        byte[] ioPinCrossings = new byte[pinCount];
        int crossingTypes = conductors + 1;
        for (int i = 0; i < ioPinCrossings.length; i ++) {
            if (ioPinIDs[i] != 0) {
                ioPinCrossings[i] = (byte) (1 + random.nextInt(conductors));
            } else {
                ioPinCrossings[i] = (byte) random.nextInt(crossingTypes);
            }
        }
        return ioPinCrossings;
    }

    private byte[][] randomClbInputCrossings() {
        byte[][] clbInputCrossings = new byte[usedClbCount][clbInputs];
        for (byte[] inputs : clbInputCrossings) {
            for (int input = 0; input < clbInputs; input ++) {
                inputs[input] = (byte) random.nextInt(conductors);
            }
        }
        return clbInputCrossings;
    }

    private byte[] randomClbOutputCrossings() {
        byte[] clbOutputCrossings = new byte[usedClbCount];
        for (int i = 0; i < clbOutputCrossings.length; i ++) {
            clbOutputCrossings[i] = (byte) random.nextInt(conductors);
        }
        return clbOutputCrossings;
    }

    private byte[][] randomSwitchBoxes() {
        byte[][] boxes = new byte[boxCount][conductors];
        for (byte[] matrix : boxes) {
            for (int i = 0; i < matrix.length; i ++) {
                //matrix[i] = (byte) random.nextInt(ELEMENT_TYPES);
                matrix[i] = 0;
            }
        }
        return boxes;
    }

    private short[] randomIDs(int size, int used) {
        short[] IDs = new short[size];
        List<Integer> freeIndexes = new ArrayList<>(size);
        for (int i = 0; i < size; i ++) {
            freeIndexes.add(i);
        }
        for (short i = 1; i <= used; i ++) {
            int randomIndex = random.nextInt(freeIndexes.size());
            int index = freeIndexes.get(randomIndex);
            freeIndexes.remove(randomIndex);
            IDs[index] = i;
        }
        return IDs;
    }
}
