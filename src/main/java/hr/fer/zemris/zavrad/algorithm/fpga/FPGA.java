package hr.fer.zemris.zavrad.algorithm.fpga;

/**
 * Class which represents a programmable FPGA chip.
 *
 * @author Mate Gašparini
 */
public class FPGA {

    /** The specified programmable I/O pins. */
    private final Pin[] pins;

    /** The specified programmable switch boxes. */
    private final SwitchBox[][] switchBoxes;

    /** The specified programmable CLBs (configurable logic blocks). */
    private final Block[][] blocks;

    /**
     * Constructor specifying the basic components.
     *
     * @param pins The specified programmable I/O pins.
     * @param switchBoxes The specified programmable switch boxes.
     * @param blocks The specified programmable CLBs.
     */
    public FPGA(Pin[] pins, SwitchBox[][] switchBoxes, Block[][] blocks) {
        this.pins = pins;
        this.switchBoxes = switchBoxes;
        this.blocks = blocks;
    }

    /**
     * Returns the specified programmable I/O pins (starting at the upper left
     * corner and going clockwise around the chip).
     *
     * @return The I/O pin array.
     */
    public Pin[] getPins() {
        return pins;
    }

    /**
     * Returns the specified programmable switch boxes (starting at the upper
     * left corner).
     *
     * @return The switch box matrix.
     */
    public SwitchBox[][] getSwitchBoxes() {
        return switchBoxes;
    }

    /**
     * Returns the specified programmable CLBs (starting at the upper left
     * corner).
     *
     * @return The CLB matrix.
     */
    public Block[][] getBlocks() {
        return blocks;
    }
}
