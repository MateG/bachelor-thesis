package hr.fer.zemris.zavrad.algorithm.solution;

import hr.fer.zemris.zavrad.algorithm.fpga.*;
import hr.fer.zemris.zavrad.algorithm.search.BFS;
import hr.fer.zemris.zavrad.algorithm.translator.SignalTranslator;
import hr.fer.zemris.zavrad.problem.Problem;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.Stack;

import static hr.fer.zemris.zavrad.algorithm.solution.EvaluatorUtil.clearFPGA;
import static hr.fer.zemris.zavrad.algorithm.solution.EvaluatorUtil.repairSwitchBoxes;
import static hr.fer.zemris.zavrad.algorithm.solution.SwitchBoxTranslator.*;
import static hr.fer.zemris.zavrad.algorithm.translator.SignalTranslator.NO_CLB;
import static hr.fer.zemris.zavrad.algorithm.translator.SignalTranslator.NO_SIGNAL;

public class Evaluator {

    private Problem problem;

    private SignalTranslator translator;

    private int maxConductor;

    private FPGA fpga;

    private Stack<Conductor> conductors = new Stack<>();

    private short currentSignal;

    /** Currently evaluating solution (to reduce stack usage). */
    private Solution solution;

    private int conductorsUsed;

    private int signalCrossings;

    private int clbViolations;

    private BFS bfs = new BFS();

    public Evaluator(Problem problem) {
        this.problem = Objects.requireNonNull(problem,
                "Problem must not be null");
        this.translator = SignalTranslator.getInstance();
        this.maxConductor = problem.getConductorsPerSegment() - 1;
        fpga = new FPGAFactory(problem).createFPGA();
    }

    public void evaluate(Solution solution) {
        this.solution = Objects.requireNonNull(solution,
                "Solution must not be null");
        this.conductorsUsed = 0;
        this.signalCrossings = 0;
        this.clbViolations = 0;
        clearFPGA(fpga);
        sendToFPGA();
        calculateFitness();
    }

    public FPGA getFpga() {
        return fpga;
    }

    private void sendToFPGA() {
        sendPins();
        sendBlocks();
        sendSwitchBoxes();
    }

    private void sendPins() {
        Pin[] pins = fpga.getPins();
        for (int pin = 0; pin < pins.length; pin ++) {
            pins[pin].setSignal(solution.ioPinIDs[pin]);
        }
    }

    private void sendBlocks() {
        Block[][] blocks = fpga.getBlocks();
        int current = 0;
        for (int row = 0, count = 0; row < blocks.length; row ++) {
            for (int col = 0; col < blocks[row].length; col ++, count ++) {
                Block block = blocks[row][col];
                short id = solution.clbIDs[count];
                if (id == NO_CLB) continue;
                block.setId(id);
                block.setInputCrossings(solution.clbInputCrossings[current]);
                block.setOutputCrossing(solution.clbOutputCrossings[current]);
                current ++;
            }
        }
    }

    private void sendSwitchBoxes() {
        repairSwitchBoxes(problem, solution.switchBoxes);
        SwitchBox[][] boxes = fpga.getSwitchBoxes();
        for (int row = 0, count = 0; row < boxes.length; row ++) {
            for (int col = 0; col < boxes[row].length; col ++, count ++) {
                SwitchBox box = boxes[row][col];
                box.setMatrix(solution.switchBoxes[count]);
            }
        }
    }

    // expand signals from pins
    // expand signals from registered CLBs
    // iterate through registered CLBs and check input validity
    // iterate through all non-input pins and check output validity

    private void calculateFitness() {
        expandPins();
        expandCLBs();
        solution.setConductorsUsed(conductorsUsed);
        solution.setSignalCrossings(signalCrossings);
        solution.setClbViolations(clbViolations);
        solution.setPinViolations(countViolatedPinConstraints());
        solution.setClbDistance(calculateCLBDistance());
    }

    private int countViolatedPinConstraints() {
        int pinConstraintsViolated = 0;
        Set<Short> signals = new HashSet<>();
        Pin[] pins = fpga.getPins();
        for (short pin = 0; pin < pins.length; pin ++) {
            Short expected = translator.getSignalConstraint(pin);
            int crossing = solution.ioPinCrossings[pin];
            if (crossing == 0) {
                if (expected == null) continue;
                pinConstraintsViolated ++;
            } else {
                short actual = pins[pin].getSegment().getConductor(crossing-1)
                        .getSignal();
                pins[pin].setSignal(actual);
                signals.add(actual);
                if (expected != null && actual != expected) {
                    pinConstraintsViolated++;
                }
            }
        }

        for (Short function : translator.getFunctionSignals()) {
            if (!signals.contains(function)) pinConstraintsViolated++;
        }
        return pinConstraintsViolated;
    }

    private int calculateCLBDistance() {
        int distance = 0;
        for (Block[] row : fpga.getBlocks()) {
            for (Block block : row) {
                short id = block.getId();
                if (id == NO_CLB) continue;
                short[] inputs = translator.getInputs(id);
                for (short input : inputs) {
                    distance += bfs.minDistance(block.getInput(), input);
                }
            }
        }
        Pin[] pins = fpga.getPins();
        for (short pin = 0; pin < pins.length; pin ++) {
            Short signal = translator.getSignalConstraint(pin);
            if (signal == null) continue;
            distance += bfs.minDistance(pins[pin].getSegment(), signal);
        }
        return distance;
    }

    private void expandPins() {
        Pin[] pins = fpga.getPins();
        for (int i = 0; i < pins.length; i ++) {
            Pin pin = pins[i];
            short signal = pin.getSignal();
            if (signal == NO_SIGNAL) continue;

            int index = solution.ioPinCrossings[i] - 1;
            if (index < 0) continue;
            Conductor start = pin.getSegment().getConductor(index);
            expandSignal(signal, start);
        }
    }

    private void expandCLBs() {
        Block[][] blocks = fpga.getBlocks();
        for (int row = 0, count = 0; row < blocks.length; row ++) {
            for (int col = 0; col < blocks[row].length; col ++, count ++) {
                Block block = blocks[row][col];
                short id = solution.clbIDs[count];
                if (id == NO_CLB) continue;

                short signal = translator.getOutput(block.getId());
                int index = block.getOutputCrossing();
                Conductor start = block.getOutput().getConductor(index);
                expandSignal(signal, start);
            }
        }

        for (int row = 0, count = 0; row < blocks.length; row ++) {
            for (int col = 0; col < blocks[row].length; col++, count++) {
                Block block = blocks[row][col];
                short id = solution.clbIDs[count];
                if (id == NO_CLB) continue;

                clbViolations += block.getInvalidCount();
                if (block.getInputCounter() != problem.getClbInputs()) {
                    clbViolations ++;
                }
            }
        }
    }

    private void expandSignal(short signal, Conductor start) {
        currentSignal = signal;
        conductors.clear();
        conductors.push(start);
        while (!conductors.isEmpty()) {
            Conductor current = conductors.pop();
            if (current.getSignal() != NO_SIGNAL) {
                signalCrossings ++;
                current.setSignal(signal); // Overwrite existing signal
                return;
            }
            conductorsUsed ++;
            current.setSignal(signal);
            Segment parent = current.getParent();
            int index = current.getIndex();

            if (parent instanceof VerticalSegment) {
                Block block = ((VerticalSegment) parent).getBlock();
                if (block != null) {
                    short id = block.getId();
                    if (id != NO_CLB) {
                        visitCLB(block, index);
                    }
                }
                expandVertically((VerticalSegment) parent, index);
            } else {
                expandHorizontally(parent, index);
            }
        }
    }

    private void visitCLB(Block block, int index) {
        if (block.getInputCounter() == problem.getClbInputs()) {
            return; // Without a doubt, the signal does not enter the CLB
        }

        byte[] inputCrossings = block.getInputCrossings();
        short[] inputs = translator.getInputs(block.getId());
        for (int i = 0; i < inputCrossings.length; i ++) {
            if (inputCrossings[i] == index) {
                // Conductor signal enters the CLB
                if (currentSignal != inputs[i]) block.invalidate();
                block.incrementInputCounter();
            }
        }
    }

    private void expandVertically(VerticalSegment parent, int index) {
        expandAbove(parent.getFrom(), index);
        expandBelow(parent.getTo(), index);
    }

    private void expandAbove(SwitchBox box, int index) {
        byte element = box.getMatrix()[maxConductor - index];
        if (isVertical(element)) {
            helperPush(box.getAbove(), index);
        }
        if (isLowerLeft(element)) {
            helperPush(box.getLeft(), maxConductor - index);
        }
        if (isLowerRight(element)) {
            helperPush(box.getRight(), maxConductor - index);
        }
    }

    private void expandBelow(SwitchBox box, int index) {
        byte element = box.getMatrix()[maxConductor - index];
        if (isVertical(element)) {
            helperPush(box.getBelow(), index);
        }
        if (isUpperLeft(element)) {
            helperPush(box.getLeft(), maxConductor - index);
        }
        if (isUpperRight(element)) {
            helperPush(box.getRight(), maxConductor - index);
        }
    }

    private void expandHorizontally(Segment parent, int index) {
        expandLeft(parent.getFrom(), index);
        expandRight(parent.getTo(), index);
    }

    private void expandLeft(SwitchBox box, int index) {
        byte element = box.getMatrix()[index];
        if (isHorizontal(element)) {
            helperPush(box.getLeft(), index);
        }
        if (isUpperRight(element)) {
            helperPush(box.getAbove(), maxConductor - index);
        }
        if (isLowerRight(element)) {
            helperPush(box.getBelow(), maxConductor - index);
        }
    }

    private void expandRight(SwitchBox box, int index) {
        byte element = box.getMatrix()[index];
        if (isHorizontal(element)) {
            helperPush(box.getRight(), index);
        }
        if (isLowerLeft(element)) {
            helperPush(box.getBelow(), maxConductor - index);
        }
        if (isUpperLeft(element)) {
            helperPush(box.getAbove(), maxConductor - index);
        }
    }

    private void helperPush(Segment segment, int index) {
        if (segment == null) return;
        Conductor conductor = segment.getConductor(index);
        if (conductor.getSignal() == currentSignal) return;
        conductors.push(conductor);
    }
}
