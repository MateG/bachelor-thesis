package hr.fer.zemris.zavrad.algorithm.translator;

import hr.fer.zemris.zavrad.problem.Problem;
import hr.fer.zemris.zavrad.problem.clb.CLB;

import java.util.HashSet;
import java.util.Set;

public class SignalUtil {

    private static final String CLB_OPEN = "CLB(";

    private static final String CLB_CLOSE = ")";

    public static Set<String> inputLabels(Problem problem) {
        Set<String> inputLabels = new HashSet<>();
        for (CLB clb : problem.getBlocks()) {
            String[] inputs = clb.getInputs();
            for (String input : inputs) {
                if (input.startsWith(CLB_OPEN) && input.endsWith(CLB_CLOSE)) {
                    continue;
                }
                inputLabels.add(input);
            }
        }
        return inputLabels;
    }
}
