package hr.fer.zemris.zavrad.algorithm.search;

import hr.fer.zemris.zavrad.algorithm.fpga.Conductor;
import hr.fer.zemris.zavrad.algorithm.fpga.FPGA;
import hr.fer.zemris.zavrad.algorithm.fpga.Segment;

import java.util.*;

import static hr.fer.zemris.zavrad.algorithm.translator.SignalTranslator.NO_SIGNAL;

public class DFS {

    private Stack<Segment> open = new Stack<>();

    private Set<Segment> closed = new HashSet<>();

    public List<Conductor> usedConductors(FPGA fpga) {
        List<Conductor> usedConductors = new ArrayList<>();

        open.clear();
        Segment start = fpga.getPins()[0].getSegment();
        open.push(start);
        closed.clear();
        closed.add(start);
        while (!open.isEmpty()) {
            Segment current = open.pop();
            for (Conductor conductor : current.getConductors()) {
                if (conductor.getSignal() == NO_SIGNAL) continue;
                usedConductors.add(conductor);
            }

            for (Segment successor : SearchUtil.successors(current)) {
                if (closed.contains(successor)) continue;
                open.push(successor);
                closed.add(successor);
            }
        }

        return usedConductors;
    }
}
