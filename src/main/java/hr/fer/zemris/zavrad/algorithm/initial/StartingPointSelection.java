package hr.fer.zemris.zavrad.algorithm.initial;

import hr.fer.zemris.zavrad.random.RandomSingleton;

import java.util.List;
import java.util.Random;

public class StartingPointSelection {

    private Random random = RandomSingleton.getInstance();

    public StartingPointSolution select(List<StartingPointSolution> population) {
        int size = population.size()-1;
        int rankSum = size * (1 + size) / 2;

        int value = random.nextInt(rankSum);
        int difference = size;
        for (int i = 0; i < size; i ++) {
            value -= difference;
            if (value < 0) return population.get(i);
            difference --;
        }
        return null;
    }
}
