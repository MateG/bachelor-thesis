package hr.fer.zemris.zavrad.algorithm.solution;

import java.util.Arrays;
import java.util.Comparator;

public class Solution implements Comparable<Solution> {

    public static final Comparator<Solution> INITIAL = Comparator
            .comparingInt(Solution::getCost);

    public static final Comparator<Solution> FINAL = Comparator
            .comparingInt(Solution::getCost)
            .thenComparingInt(Solution::getConductorsUsed);

    /** IDs of all CLBs, starting from the upper left corner; 0 is no CLB. */
    short[] clbIDs;

    /**
     * Input/output IDs on each of the I/O pins, starting at the upper left
     * corner and going clockwise around the chip.
     */
    short[] ioPinIDs;

    /** Indexes of the segments intersecting the I/O pins. */
    byte[] ioPinCrossings;

    /** Indexes of the segments intersecting the CLB inputs. */
    byte[][] clbInputCrossings;

    /** Indexes of the segments intersecting the CLB outputs. */
    byte[] clbOutputCrossings;

    /** Array of switch box matrices mapping the connections. */
    byte[][] switchBoxes;

    private int conductorsUsed;

    private int signalCrossings;

    private int clbViolations;

    private int pinViolations;

    private int clbDistance;

    public Solution(short[] clbIDs, short[] ioPinIDs,
            byte[] ioPinCrossings, byte[][] clbInputCrossings,
            byte[] clbOutputCrossings, byte[][] switchBoxes) {
        this.clbIDs = clbIDs;
        this.ioPinIDs = ioPinIDs;
        this.ioPinCrossings = ioPinCrossings;
        this.clbInputCrossings = clbInputCrossings;
        this.clbOutputCrossings = clbOutputCrossings;
        this.switchBoxes = switchBoxes;
    }

    public short[] getClbIDs() {
        return clbIDs;
    }

    public short[] getIoPinIDs() {
        return ioPinIDs;
    }

    public byte[] getIoPinCrossings() {
        return ioPinCrossings;
    }

    public byte[][] getClbInputCrossings() {
        return clbInputCrossings;
    }

    public byte[] getClbOutputCrossings() {
        return clbOutputCrossings;
    }

    public byte[][] getSwitchBoxes() {
        return switchBoxes;
    }

    public int getConductorsUsed() {
        return conductorsUsed;
    }

    public void setConductorsUsed(int conductorsUsed) {
        this.conductorsUsed = conductorsUsed;
    }

    public int getSignalCrossings() {
        return signalCrossings;
    }

    public void setSignalCrossings(int signalCrossings) {
        this.signalCrossings = signalCrossings;
    }

    public int getClbViolations() {
        return clbViolations;
    }

    public void setClbViolations(int clbViolations) {
        this.clbViolations = clbViolations;
    }

    public int getPinViolations() {
        return pinViolations;
    }

    public void setPinViolations(int pinViolations) {
        this.pinViolations = pinViolations;
    }

    public int getClbDistance() {
        return clbDistance;
    }

    public void setClbDistance(int clbDistance) {
        this.clbDistance = clbDistance;
    }

    public int getCost() {
        return 2*signalCrossings + clbViolations + pinViolations + clbDistance;
    }

    public boolean isAcceptable() {
        return signalCrossings == 0 && clbViolations == 0 && pinViolations == 0;
    }

    public void overwrite(Solution to) {
        System.arraycopy(this.clbIDs, 0, to.clbIDs, 0, clbIDs.length);
        System.arraycopy(this.ioPinIDs, 0, to.ioPinIDs, 0, ioPinIDs.length);
        System.arraycopy(this.ioPinCrossings, 0, to.ioPinCrossings, 0,
                ioPinCrossings.length);
        matrixCopy(this.clbInputCrossings, to.clbInputCrossings);
        System.arraycopy(this.clbOutputCrossings, 0, to.clbOutputCrossings, 0,
                clbOutputCrossings.length);
        matrixCopy(this.switchBoxes, to.switchBoxes);
    }

    @Override
    public int compareTo(Solution other) {
        return INITIAL.compare(this, other);
    }

    @Override
    public String toString() {
        return "clbIDs\n" + Arrays.toString(clbIDs) +
                "\nioPinIDs\n" + Arrays.toString(ioPinIDs) +
                "\nioPinCrossings\n" + Arrays.toString(ioPinCrossings) +
                "\nclbInputCrossings\n" + Arrays.toString(clbInputCrossings) +
                "\nclbOutputCrossings\n" + Arrays.toString(clbOutputCrossings) +
                "\nswitchBoxes" + Arrays.toString(switchBoxes);
    }

    private void matrixCopy(byte[][] from, byte[][] to) {
        for (int i = 0; i < to.length; i ++) {
            System.arraycopy(from[i], 0, to[i], 0, to[i].length);
        }
    }
}
