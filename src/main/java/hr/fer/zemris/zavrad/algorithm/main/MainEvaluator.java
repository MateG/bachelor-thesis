package hr.fer.zemris.zavrad.algorithm.main;

import hr.fer.zemris.zavrad.algorithm.solution.Dependency;
import hr.fer.zemris.zavrad.algorithm.initial.StartingPointSolution;
import hr.fer.zemris.zavrad.algorithm.fpga.*;
import hr.fer.zemris.zavrad.algorithm.search.BFS;
import hr.fer.zemris.zavrad.algorithm.translator.SignalTranslator;
import hr.fer.zemris.zavrad.problem.Problem;

import java.util.*;

import static hr.fer.zemris.zavrad.algorithm.solution.EvaluatorUtil.*;
import static hr.fer.zemris.zavrad.algorithm.solution.SwitchBoxTranslator.*;
import static hr.fer.zemris.zavrad.algorithm.solution.SwitchBoxTranslator.isUpperLeft;
import static hr.fer.zemris.zavrad.algorithm.translator.SignalTranslator.NO_CLB;
import static hr.fer.zemris.zavrad.algorithm.translator.SignalTranslator.NO_SIGNAL;

public class MainEvaluator {

    private Problem problem;

    private short[] inputPinIDs;

    private List<Pin> inputPins;

    private List<Pin> functionPins;

    private List<Short> functionPinSignals;

    private List<Block> activeBlocks;

    private SignalTranslator translator;

    private int maxConductor;

    private FPGA fpga;

    private Stack<Conductor> conductors = new Stack<>();

    private short currentSignal;

    private BFS bfs = new BFS();

    /** Currently evaluating solution (to reduce stack usage). */
    private MainSolution solution;

    private int signalClashes;

    private int conductorsUsed;

    public MainEvaluator(Problem problem, StartingPointSolution starting) {
        this.problem = problem;
        this.translator = SignalTranslator.getInstance();
        this.maxConductor = problem.getConductorsPerSegment() - 1;
        fpga = new FPGAFactory(problem).createFPGA();
        setUpPins(starting.getIoPinIDs());
        setUpCLBs(starting.getClbIDs());
        sendPins();
    }

    public void evaluate(MainSolution solution) {
        this.solution = solution;
        this.signalClashes = 0;
        this.conductorsUsed = 0;
        clearSegments(fpga);
        clearBlocks();
        sendBlocks();
        sendSwitchBoxes();
        calculateFitness();
    }

    public void updateUnsatisfiedWeights(MainSolution solution,
            Map<Dependency, Integer> unsatisfiedWeights) {
        evaluate(solution);
        calculateDistanceWeights(unsatisfiedWeights);
    }

    public FPGA getFpga() {
        return fpga;
    }

    public List<Pin> getFunctionPins() {
        return functionPins;
    }

    public List<Short> getFunctionPinSignals() {
        return functionPinSignals;
    }

    public List<Block> getActiveBlocks() {
        return activeBlocks;
    }

    public short[] getInputPinIDs() {
        return inputPinIDs;
    }

    private void setUpPins(short[] ioPinIDs) {
        inputPinIDs = new short[ioPinIDs.length];
        inputPins = new ArrayList<>();
        functionPins = new ArrayList<>();
        functionPinSignals = new ArrayList<>();
        Pin[] pins = fpga.getPins();
        for (short i = 0; i < inputPinIDs.length; i ++) {
            short id = ioPinIDs[i];
            if (id == NO_SIGNAL) continue;

            if (translator.isInputSignal(id)) {
                inputPinIDs[i] = id;
                inputPins.add(pins[i]);
                pins[i].setSignal(id);
            } else {
                functionPins.add(pins[i]);
                functionPinSignals.add(id);
            }
        }
    }

    private void setUpCLBs(short[] clbIDs) {
        activeBlocks = new ArrayList<>();

        Block[][] blocks = fpga.getBlocks();
        short count = 0;
        for (int row = 0; row < blocks.length; row ++) {
            for (int col = 0; col < blocks[row].length; col ++, count ++) {
                Block block = blocks[row][col];
                short id = clbIDs[count];
                if (id == NO_CLB) continue;

                block.setId(id);
                activeBlocks.add(block);
            }
        }
    }

    private void sendPins() {
        Pin[] pins = fpga.getPins();
        for (int pin = 0; pin < pins.length; pin ++) {
            pins[pin].setSignal(inputPinIDs[pin]);
        }
    }

    private void sendBlocks() {
        int i = 0;
        for (Block block : activeBlocks) {
            block.setInputCrossings(solution.clbInputCrossings[i]);
            block.setOutputCrossing(solution.clbOutputCrossings[i]);
            i ++;
        }
    }

    private void sendSwitchBoxes() {
        repairSwitchBoxes(problem, solution.switchBoxes);
        SwitchBox[][] boxes = fpga.getSwitchBoxes();
        for (int row = 0, count = 0; row < boxes.length; row ++) {
            for (int col = 0; col < boxes[row].length; col ++, count ++) {
                SwitchBox box = boxes[row][col];
                box.setMatrix(solution.switchBoxes[count]);
            }
        }
    }

    private void calculateFitness() {
        expandPins();
        expandCLBs();
        solution.setSignalClashes(signalClashes);
        solution.setDistanceCost(calculateDistanceCost());
        solution.setConductorsUsed(conductorsUsed);
    }

    private int calculateDistanceCost() {
        int distance = 0;
        for (Block block : activeBlocks) {
            short[] inputs = translator.getInputs(block.getId());
            for (short input : inputs) {
                distance += bfs.minDistance(block.getInput(), input);
            }
        }

        int i = 0;
        for (Pin functionPin : functionPins) {
            short signal = functionPinSignals.get(i++);
            distance += bfs.minDistance(functionPin.getSegment(), signal);
        }
        return distance;
    }

    private void calculateDistanceWeights(Map<Dependency, Integer> unsatisfiedWeights) {
        for (Block block : activeBlocks) {
            short id = block.getId();
            short[] inputs = translator.getInputs(id);
            short output = translator.getOutput(id);
            for (short input : inputs) {
                int distance = bfs.minDistance(block.getInput(), input);
                if (distance == 0) continue;

                Dependency dependency = new Dependency(input, output);
                unsatisfiedWeights.merge(dependency, 1, (a, b) -> a + b);
            }
        }

        int i = 0;
        for (Pin functionPin : functionPins) {
            short signal = functionPinSignals.get(i);
            int distance = bfs.minDistance(functionPin.getSegment(), signal);
            if (distance == 0) {
                i ++;
                continue;
            }

            Dependency dependency = new Dependency(signal, signal);
            unsatisfiedWeights.merge(dependency, 1, (a, b) -> a + b);
            i ++;
        }
    }

    private void expandPins() {
        Pin[] pins = fpga.getPins();
        for (int i = 0; i < pins.length; i ++) {
            Pin pin = pins[i];
            short signal = pin.getSignal();
            if (signal == NO_SIGNAL) continue;

            int index = solution.ioPinCrossings[i] - 1;
            if (index < 0) continue;
            Conductor start = pin.getSegment().getConductor(index);
            expandSignal(signal, start);
        }
    }

    private void expandCLBs() {
        for (Block block : activeBlocks) {
            short signal = translator.getOutput(block.getId());
            int conductorIndex = block.getOutputCrossing();
            Conductor start = block.getOutput().getConductor(conductorIndex);
            expandSignal(signal, start);
        }
    }

    private void expandSignal(short signal, Conductor start) {
        currentSignal = signal;
        conductors.clear();
        conductors.push(start);
        while (!conductors.isEmpty()) {
            Conductor current = conductors.pop();
            if (current.getSignal() != NO_SIGNAL) {
                signalClashes++;
                current.setSignal(signal); // Overwrite existing signal
                return;
            }

            conductorsUsed ++;
            current.setSignal(signal);
            Segment parent = current.getParent();
            int index = current.getIndex();

            if (parent instanceof VerticalSegment) {
                Block block = ((VerticalSegment) parent).getBlock();
                if (block != null) {
                    short id = block.getId();
                    if (id != NO_CLB) {
                        visitCLB(block, index);
                    }
                }
                expandVertically((VerticalSegment) parent, index);
            } else {
                expandHorizontally(parent, index);
            }
        }
    }

    private void visitCLB(Block block, int index) {
        if (block.getInputCounter() == problem.getClbInputs()) {
            return; // Without a doubt, the signal does not enter the CLB
        }

        byte[] inputCrossings = block.getInputCrossings();
        short[] inputs = translator.getInputs(block.getId());
        for (int i = 0; i < inputCrossings.length; i ++) {
            if (inputCrossings[i] == index) {
                // Conductor signal enters the CLB
                if (currentSignal != inputs[i]) block.invalidate();
                block.incrementInputCounter();
            }
        }
    }

    private void expandVertically(VerticalSegment parent, int index) {
        expandAbove(parent.getFrom(), index);
        expandBelow(parent.getTo(), index);
    }

    private void expandAbove(SwitchBox box, int index) {
        byte element = box.getMatrix()[maxConductor - index];
        if (isVertical(element)) {
            helperPush(box.getAbove(), index);
        }
        if (isLowerLeft(element)) {
            helperPush(box.getLeft(), maxConductor - index);
        }
        if (isLowerRight(element)) {
            helperPush(box.getRight(), maxConductor - index);
        }
    }

    private void expandBelow(SwitchBox box, int index) {
        byte element = box.getMatrix()[maxConductor - index];
        if (isVertical(element)) {
            helperPush(box.getBelow(), index);
        }
        if (isUpperLeft(element)) {
            helperPush(box.getLeft(), maxConductor - index);
        }
        if (isUpperRight(element)) {
            helperPush(box.getRight(), maxConductor - index);
        }
    }

    private void expandHorizontally(Segment parent, int index) {
        expandLeft(parent.getFrom(), index);
        expandRight(parent.getTo(), index);
    }

    private void expandLeft(SwitchBox box, int index) {
        byte element = box.getMatrix()[index];
        if (isHorizontal(element)) {
            helperPush(box.getLeft(), index);
        }
        if (isUpperRight(element)) {
            helperPush(box.getAbove(), maxConductor - index);
        }
        if (isLowerRight(element)) {
            helperPush(box.getBelow(), maxConductor - index);
        }
    }

    private void expandRight(SwitchBox box, int index) {
        byte element = box.getMatrix()[index];
        if (isHorizontal(element)) {
            helperPush(box.getRight(), index);
        }
        if (isLowerLeft(element)) {
            helperPush(box.getBelow(), maxConductor - index);
        }
        if (isUpperLeft(element)) {
            helperPush(box.getAbove(), maxConductor - index);
        }
    }

    private void helperPush(Segment segment, int index) {
        if (segment == null) return;
        Conductor conductor = segment.getConductor(index);
        if (conductor.getSignal() == currentSignal) return;
        conductors.push(conductor);
    }

    private void clearBlocks() {
        for (Block block : activeBlocks) {
            block.setInputCrossings(null);
            block.resetInputCounter();
            block.validate();
        }
    }
}
