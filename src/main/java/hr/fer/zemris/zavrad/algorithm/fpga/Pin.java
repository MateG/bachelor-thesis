package hr.fer.zemris.zavrad.algorithm.fpga;

/**
 * {@link FPGA} component which represents a programmable I/O pin.
 *
 * @author Mate Gašparini
 */
public class Pin {

    /** The corresponding segment reference. */
    Segment segment;

    /** The currently active signal; 1-indexed; 0 is no signal. */
    private short signal;

    /**
     * Returns the corresponding segment reference (specified during FPGA
     * construction).
     *
     * @return The corresponding segment.
     */
    public Segment getSegment() {
        return segment;
    }

    /**
     * Returns the currently active signal. Zero denotes no signal.
     *
     * @return The signal value.
     */
    public short getSignal() {
        return signal;
    }

    /**
     * Sets the currently active signal to the given value. Zero denotes no
     * signal.
     *
     * @param signal The given value.
     */
    public void setSignal(short signal) {
        this.signal = signal;
    }
}
