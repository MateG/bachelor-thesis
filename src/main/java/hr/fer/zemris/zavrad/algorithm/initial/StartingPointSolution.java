package hr.fer.zemris.zavrad.algorithm.initial;

import java.util.Arrays;

public class StartingPointSolution implements Comparable<StartingPointSolution> {

    short[] ioPinIDs;

    short[] clbIDs;

    int cost;

    boolean acceptable;

    public StartingPointSolution(short[] ioPinIDs, short[] clbIDs) {
        this.ioPinIDs = ioPinIDs;
        this.clbIDs = clbIDs;
    }

    public short[] getIoPinIDs() {
        return ioPinIDs;
    }

    public short[] getClbIDs() {
        return clbIDs;
    }

    public void overwrite(StartingPointSolution to) {
        System.arraycopy(this.ioPinIDs, 0, to.ioPinIDs, 0, ioPinIDs.length);
        System.arraycopy(this.clbIDs, 0, to.clbIDs, 0, clbIDs.length);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StartingPointSolution that = (StartingPointSolution) o;
        return Arrays.equals(ioPinIDs, that.ioPinIDs) &&
                Arrays.equals(clbIDs, that.clbIDs);
    }

    @Override
    public int hashCode() {
        int result = Arrays.hashCode(ioPinIDs);
        result = 31 * result + Arrays.hashCode(clbIDs);
        return result;
    }

    @Override
    public int compareTo(StartingPointSolution o) {
        return Integer.compare(this.cost, o.cost);
    }
}
