package hr.fer.zemris.zavrad.algorithm.initial;

import hr.fer.zemris.zavrad.algorithm.translator.SignalTranslator;
import hr.fer.zemris.zavrad.random.RandomSingleton;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static hr.fer.zemris.zavrad.algorithm.translator.SignalTranslator.NO_SIGNAL;

public class StartingPointMutation {

    private Random random = RandomSingleton.getInstance();

    private SignalTranslator translator = SignalTranslator.getInstance();

    public void mutate(StartingPointSolution solution) {
        if (random.nextBoolean()) {
            short[] clbIDs = solution.clbIDs;
            int firstIndex = random.nextInt(clbIDs.length);
            int secondIndex;
            if (clbIDs[firstIndex] == 0) {
                do {
                    secondIndex = random.nextInt(clbIDs.length);
                } while (clbIDs[secondIndex] == 0);
            } else {
                do {
                    secondIndex = random.nextInt(clbIDs.length);
                } while (firstIndex == secondIndex);
            }

            short firstCopy = clbIDs[firstIndex];
            clbIDs[firstIndex] = clbIDs[secondIndex];
            clbIDs[secondIndex] = firstCopy;
        } else {
            short[] pins = solution.ioPinIDs;
            List<Short> freePins = new ArrayList<>();
            for (short i = 0; i < pins.length; i ++) {
                if (pins[i] == NO_SIGNAL) continue;
                if (translator.getSignalConstraint(i) == null) {
                    freePins.add(i);
                }
            }
            int firstIndex = random.nextInt(freePins.size());
            short first = freePins.get(firstIndex);
            freePins.remove(firstIndex);

            for (short i = 0; i < pins.length; i ++) {
                if (pins[i] == NO_SIGNAL) freePins.add(i);
            }
            short second = freePins.get(random.nextInt(freePins.size()));

            short copy = pins[first];
            pins[first] = pins[second];
            pins[second] = copy;
        }
    }
}
