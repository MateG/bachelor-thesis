package hr.fer.zemris.zavrad.algorithm.initial;

import hr.fer.zemris.zavrad.algorithm.translator.SignalTranslator;
import hr.fer.zemris.zavrad.problem.Problem;
import hr.fer.zemris.zavrad.random.RandomSingleton;

import java.util.*;

public class StartingPointSolutionGenerator {

    private Random random = RandomSingleton.getInstance();

    private SignalTranslator translator = SignalTranslator.getInstance();

    private Set<Short> namedSignals;

    private int pinCount;

    private int signalCount;

    private int clbCount;

    private int usedClbCount;

    public StartingPointSolutionGenerator(Problem problem) {
        pinCount = 2 * (problem.getClbRows() + problem.getClbColumns())
                * problem.getPinsPerSegment();
        signalCount = translator.getInputSignals().size()
                + translator.getFunctionSignals().size();
        clbCount = problem.getClbRows()*problem.getClbColumns();
        usedClbCount = problem.getBlocks().size();
        namedSignals = new HashSet<>(signalCount);
        namedSignals.addAll(translator.getInputSignals());
        namedSignals.addAll(translator.getFunctionSignals());
    }

    public StartingPointSolution randomSolution() {
        return new StartingPointSolution(randomIoPinIDs(), randomClbIDs());
    }

    public StartingPointSolution emptySolution() {
        return new StartingPointSolution(
                new short[pinCount], new short[clbCount]);
    }

    private short[] randomIoPinIDs() {
        short[] ioPinIDs = new short[pinCount];
        List<Short> freePins = new ArrayList<>(pinCount);
        for (short pin = 0; pin < pinCount; pin ++) {
            freePins.add(pin);
        }
        List<Short> freeSignals = new ArrayList<>(signalCount);
        for (Short signal : namedSignals) {
            Short pinConstraint = translator.getPinIndexConstraint(signal);
            if (pinConstraint == null) {
                freeSignals.add(signal);
            } else {
                ioPinIDs[pinConstraint] = signal;
                freePins.remove(pinConstraint);
            }
        }
        while (!freeSignals.isEmpty()) {
            int signalIndex = random.nextInt(freeSignals.size());
            short signal = freeSignals.get(signalIndex);
            freeSignals.remove(signalIndex);
            int pinIndex = random.nextInt(freePins.size());
            short pin = freePins.get(pinIndex);
            freePins.remove(pinIndex);
            ioPinIDs[pin] = signal;
        }
        return ioPinIDs;
    }

    private short[] randomClbIDs() {
        short[] IDs = new short[clbCount];
        List<Integer> freeIndexes = new ArrayList<>(clbCount);
        for (int i = 0; i < clbCount; i ++) {
            freeIndexes.add(i);
        }
        for (short i = 1; i <= usedClbCount; i ++) {
            int randomIndex = random.nextInt(freeIndexes.size());
            int index = freeIndexes.get(randomIndex);
            freeIndexes.remove(randomIndex);
            IDs[index] = i;
        }
        return IDs;
    }
}
