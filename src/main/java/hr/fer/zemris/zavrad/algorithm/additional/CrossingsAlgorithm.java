package hr.fer.zemris.zavrad.algorithm.additional;

import hr.fer.zemris.zavrad.algorithm.Algorithm;
import hr.fer.zemris.zavrad.algorithm.fpga.Block;
import hr.fer.zemris.zavrad.algorithm.fpga.Conductor;
import hr.fer.zemris.zavrad.algorithm.fpga.FPGA;
import hr.fer.zemris.zavrad.algorithm.fpga.Pin;
import hr.fer.zemris.zavrad.algorithm.main.MainEvaluator;
import hr.fer.zemris.zavrad.algorithm.main.MainSolution;
import hr.fer.zemris.zavrad.algorithm.translator.SignalTranslator;

import java.util.List;

public class CrossingsAlgorithm implements Algorithm<MainSolution> {

    private MainEvaluator evaluator;

    private FPGA fpga;

    private MainSolution solution;

    private List<Pin> functionPins;

    private List<Short> functionPinSignals;

    private List<Block> activeBlocks;

    public CrossingsAlgorithm(MainEvaluator evaluator, MainSolution solution,
            List<Pin> functionPins, List<Short> functionPinSignals,
            List<Block> activeBlocks) {
        this.evaluator = evaluator;
        this.solution = solution;
        this.functionPins = functionPins;
        this.functionPinSignals = functionPinSignals;
        this.activeBlocks = activeBlocks;
    }

    @Override
    public MainSolution solve() {
        evaluator.evaluate(solution);
        fpga = evaluator.getFpga();
        solveOutputPinCrossings(solution.getIoPinCrossings());
        solveCLBInputCrossings(solution.getClbInputCrossings());
        return solution;
    }

    private void solveOutputPinCrossings(byte[] ioPinCrossings) {
        int functionPinIndex = 0;
        Pin[] pins = fpga.getPins();
        for (int i = 0; i < pins.length; i ++) {
            if (functionPinIndex >= functionPins.size()) break;
            Pin pin = pins[i];
            Pin functionPin = functionPins.get(functionPinIndex);
            if (functionPin != pin) continue;

            Short signal = functionPinSignals.get(functionPinIndex++);
            for (Conductor conductor : pin.getSegment().getConductors()) {
                if (conductor.getSignal() == signal) {
                    ioPinCrossings[i] = (byte) (conductor.getIndex() + 1);
                    break;
                }
            }
        }
    }

    private void solveCLBInputCrossings(byte[][] clbInputCrossings) {
        SignalTranslator translator = SignalTranslator.getInstance();
        int i = 0;
        for (Block block : activeBlocks) {
            byte[] crossings = clbInputCrossings[i++];
            int inputIndex = 0;
            for (short input : translator.getInputs(block.getId())) {
                for (Conductor conductor : block.getInput().getConductors()) {
                    if (conductor.getSignal() == input) {
                        crossings[inputIndex++] = (byte) conductor.getIndex();
                        break;
                    }
                }
            }
        }
    }
}
