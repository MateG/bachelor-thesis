package hr.fer.zemris.zavrad.algorithm.fpga;

import java.util.Arrays;
import java.util.Objects;

/**
 * {@link FPGA} component which represents a group of {@link Conductor}s linked
 * from one {@link SwitchBox} to another.
 *
 * @author Mate Gašparini
 */
public class Segment {

    /** The specified (upper or left) corresponding switch box. */
    private final SwitchBox from;

    /** The specified (lower or right) corresponding switch box. */
    private final SwitchBox to;

    /** The specified array of conductive elements. */
    private final Conductor[] conductors;

    /**
     * Constructor specifying the switch box references and conductor count.
     *
     * @param from The specified (upper of left) switch box.
     * @param to The specified (lower or right) switch box.
     * @param conductorCount The specified conductor count.
     */
    public Segment(SwitchBox from, SwitchBox to, int conductorCount) {
        this.from = from;
        this.to = to;
        this.conductors = new Conductor[conductorCount];
        for (int i = 0; i < conductors.length; i ++) {
            this.conductors[i] = new Conductor(this, i);
        }
    }

    /**
     * Returns the upper or left switch box (specified during FPGA
     * construction).
     *
     * @return The corresponding switch box.
     */
    public SwitchBox getFrom() {
        return from;
    }

    /**
     * Returns the lower or right switch box (specified during FPGA
     * construction).
     *
     * @return The corresponding switch box.
     */
    public SwitchBox getTo() {
        return to;
    }

    /**
     * Returns the inner conductor array.
     *
     * @return Array of references to the contained conductors.
     */
    public Conductor[] getConductors() {
        return conductors;
    }

    /**
     * Returns the inner conductor at the specified index.
     *
     * @param index The specified index.
     * @return The corresponding conductor.
     */
    public Conductor getConductor(int index) {
        return conductors[index];
    }

    /**
     * Sets all conductor signals to zero (no signal).
     */
    public void resetSignals() {
        for (Conductor conductor : conductors) {
            conductor.setSignal((short) 0);
        }
    }

    public boolean containsSignal(short signal) {
        for (Conductor conductor : conductors) {
            if (conductor.getSignal() == signal) return true;
        }
        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Segment segment = (Segment) o;
        return from.equals(segment.from) &&
                to.equals(segment.to) &&
                Arrays.equals(conductors, segment.conductors);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(from, to);
        result = 31 * result + Arrays.hashCode(conductors);
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Conductor conductor : conductors) {
            sb.append(conductor.getSignal()).append(' ');
        }
        return sb.toString();
    }
}
