package hr.fer.zemris.zavrad.algorithm.solution;

import hr.fer.zemris.zavrad.algorithm.fpga.Block;
import hr.fer.zemris.zavrad.algorithm.fpga.FPGA;
import hr.fer.zemris.zavrad.algorithm.fpga.Segment;
import hr.fer.zemris.zavrad.algorithm.fpga.SwitchBox;
import hr.fer.zemris.zavrad.problem.architecture.Architecture;

import static hr.fer.zemris.zavrad.algorithm.solution.SwitchBoxTranslator.*;
import static hr.fer.zemris.zavrad.algorithm.translator.SignalTranslator.NO_CLB;

public final class EvaluatorUtil {

    /**
     * Default private constructor.
     */
    private EvaluatorUtil() {
    }

    public static void repairSwitchBoxes(Architecture problem,
            byte[][] switchBoxes) {
        int cols = problem.getClbColumns()+1;
        // Disconnect above
        for (int col = 0; col < cols; col ++) {
            byte[] matrix = switchBoxes[col];
            for (int i = 0; i < matrix.length; i ++) {
                matrix[i] = UPPER_DISCONNECTS[matrix[i]];
            }
        }
        int clbRows = problem.getClbRows();
        // Disconnect below
        for (int i = 0, c = cols*clbRows; i < cols; i ++, c++) {
            byte[] matrix = switchBoxes[c];
            for (int j = 0; j < matrix.length; j ++) {
                matrix[j] = LOWER_DISCONNECTS[matrix[j]];
            }
        }
        // Disconnect left
        for (int i = 0, c = 0; i < clbRows; i ++, c+= cols) {
            byte[] matrix = switchBoxes[c];
            for (int j = 0; j < matrix.length; j ++) {
                matrix[j] = LEFT_DISCONNECTS[matrix[j]];
            }
        }
        int clbColumns = problem.getClbColumns();
        // Disconnect right
        for (int i = 0, c = clbColumns; i < clbRows; i ++, c += cols) {
            byte[] matrix = switchBoxes[c];
            for (int j = 0; j < matrix.length; j ++) {
                matrix[j] = RIGHT_DISCONNECTS[matrix[j]];
            }
        }
    }

    public static void clearFPGA(FPGA fpga) {
        clearBlocks(fpga);
        clearSegments(fpga);
    }

    private static void clearBlocks(FPGA fpga) {
        for (Block[] row : fpga.getBlocks()) {
            for (Block block : row) {
                block.setId(NO_CLB);
                block.setInputCrossings(null);
                block.resetInputCounter();
                block.validate();
            }
        }
    }

    public static void clearSegments(FPGA fpga) {
        for (SwitchBox[] row : fpga.getSwitchBoxes()) {
            for (SwitchBox box : row) {
                Segment right = box.getRight();
                if (right != null) right.resetSignals();
                Segment below = box.getBelow();
                if (below != null) below.resetSignals();
            }
        }
    }
}
