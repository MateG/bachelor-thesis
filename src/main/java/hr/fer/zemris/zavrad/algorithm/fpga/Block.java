package hr.fer.zemris.zavrad.algorithm.fpga;

/**
 * {@link FPGA} component which represents a programmable CLB (configurable
 * logic block).
 *
 * @author Mate Gašparini
 */
public class Block {

    /** Input segment reference. */
    Segment input;

    /** Output segment reference. */
    Segment output;

    /** Programmable unique identifier of the CLB; 1-indexed; 0 is no CLB. */
    private short id;

    /** Programmable input segment crossings; 0-indexed. */
    private byte[] inputCrossings;

    /** Programmable output segment crossing; 0-indexed. */
    private byte outputCrossing;

    /** Counts the inputs currently linked to the CLB. */
    private byte inputCounter;

    /** Counter that indicates the (in)validity of linked inputs. */
    private int invalidCount;

    /**
     * Returns the input segment reference (specified during FPGA
     * construction).
     *
     * @return Input to the CLB.
     */
    public Segment getInput() {
        return input;
    }

    /**
     * Returns the output segment reference (specified during FPGA
     * construction).
     *
     * @return Output from the CLB.
     */
    public Segment getOutput() {
        return output;
    }

    /**
     * Returns the current CLB identifier. Zero denotes a non-programmed CLB.
     *
     * @return ID of the CLB.
     */
    public short getId() {
        return id;
    }

    /**
     * Sets the ID to the given value. Zero denotes a non-programmed CLB.
     *
     * @param id The given value.
     */
    public void setId(short id) {
        this.id = id;
    }

    /**
     * Returns the current input segment crossings (0-indexed).
     *
     * @return The CLB input crossings.
     */
    public byte[] getInputCrossings() {
        return inputCrossings;
    }

    /**
     * Sets the input segment crossings to the given array (0-indexed).
     *
     * @param inputCrossings The given array.
     */
    public void setInputCrossings(byte[] inputCrossings) {
        this.inputCrossings = inputCrossings;
    }

    /**
     * Returns the current output segment crossing (0-indexed).
     *
     * @return The CLB output crossing.
     */
    public byte getOutputCrossing() {
        return outputCrossing;
    }

    /**
     * Sets the output crossing to the given value (0-indexed).
     *
     * @param outputCrossing The given value.
     */
    public void setOutputCrossing(byte outputCrossing) {
        this.outputCrossing = outputCrossing;
    }

    /**
     * Returns the count of inputs currently linked to the CLB.
     *
     * @return The current input counter.
     */
    public byte getInputCounter() {
        return inputCounter;
    }

    /**
     * Resets the current CLB input counter.
     */
    public void resetInputCounter() {
        this.inputCounter = 0;
    }

    /**
     * Increments the current CLB input counter by 1.
     */
    public void incrementInputCounter() {
        this.inputCounter ++;
    }

    /**
     * Returns the number of invalid CLB inputs.
     *
     * @return The invalidity counter value.
     */
    public int getInvalidCount() {
        return invalidCount;
    }

    /**
     * Resets the invalidity.
     */
    public void validate() {
        this.invalidCount = 0;
    }

    /**
     * Increments the invalidity counter.
     */
    public void invalidate() {
        this.invalidCount ++;
    }
}
