package hr.fer.zemris.zavrad.algorithm.annealing;

public interface TempSchedule {

    double getNextTemperature();

    int getInnerLoopCounter();

    int getOuterLoopCounter();
}
