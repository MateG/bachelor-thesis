package hr.fer.zemris.zavrad.algorithm.solution;

import hr.fer.zemris.zavrad.algorithm.fpga.FPGA;
import hr.fer.zemris.zavrad.algorithm.fpga.Pin;
import hr.fer.zemris.zavrad.algorithm.translator.SignalTranslator;
import hr.fer.zemris.zavrad.problem.Problem;
import hr.fer.zemris.zavrad.problem.clb.CLB;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;

import static hr.fer.zemris.zavrad.algorithm.translator.SignalTranslator.NO_CLB;
import static hr.fer.zemris.zavrad.algorithm.translator.SignalTranslator.NO_SIGNAL;

public class SolutionPrinter {

    private Solution solution;

    private Problem problem;

    private FPGA fpga;

    public SolutionPrinter(Solution solution, Problem problem) {
        this.solution = solution;
        this.problem = problem;
        Evaluator evaluator = new Evaluator(problem);
        evaluator.evaluate(solution);
        this.fpga = evaluator.getFpga();
    }

    public void writeToFile(Path path) throws IOException {
        try (BufferedOutputStream stream = new BufferedOutputStream(
                Files.newOutputStream(path))) {
            write(stream);
        }
    }

    public void writeToStdout() throws IOException {
        write(System.out);
    }

    private void write(OutputStream stream) throws IOException {
        writeCLBs(stream);
        writeIoPins(stream);
        writeSwitchBoxes(stream);
    }

    private void writeCLBs(OutputStream stream) throws IOException {
        short[] IDs = solution.clbIDs;
        byte[][] inputCrossings = solution.clbInputCrossings;
        byte[] outputCrossings = solution.clbOutputCrossings;
        List<CLB> blocks = problem.getBlocks();

        StringBuilder sb = new StringBuilder();
        sb.append("# CLB(#): (row, col)\n# IN: [in0, in1 ...]\n# OUT: out\n");
        for (int i = 0; i < IDs.length; i ++) {
            short id = IDs[i];
            if (id == NO_CLB) continue;

            CLB block = blocks.get(id-1);
            sb.append(block.getId()).append(": (")
                    .append(i / (problem.getClbColumns())).append(", ")
                    .append(i % problem.getClbColumns()).append(")\n");
            byte[] in = inputCrossings[id-1];
            sb.append("IN: ").append(Arrays.toString(in)).append('\n');
            sb.append("OUT: ").append(outputCrossings[id-1]).append('\n');
        }
        sb.append('\n');
        stream.write(sb.toString().getBytes());
    }

    private void writeIoPins(OutputStream stream) throws IOException {
        short[] IDs = solution.ioPinIDs;
        Pin[] pins = fpga.getPins();
        byte[] crossings = solution.ioPinCrossings;
        SignalTranslator translator = SignalTranslator.getInstance();

        StringBuilder sb = new StringBuilder();
        sb.append("# IO pins (starting at upper left corner; going clockwise)\n");
        for (int i = 0; i < IDs.length; i ++) {
            short id = IDs[i];
            if (id == NO_SIGNAL) {
                id = pins[i].getSignal();
                if (id == NO_SIGNAL) continue;
            }

            sb.append(translator.getSignalName(id))
                    .append(" AT ").append(i).append('\n');
            sb.append("CROSSING: ").append(crossings[i]-1).append('\n');
        }
        sb.append('\n');
        stream.write(sb.toString().getBytes());
    }

    private void writeSwitchBoxes(OutputStream stream) throws IOException {
        byte[][] switchBoxes = solution.switchBoxes;

        StringBuilder sb = new StringBuilder();
        sb.append("# Switch boxes (starting at upper left corner; line by line)\n");
        for (int i = 0; i < switchBoxes.length; i ++) {
            sb.append('(').append(i / (problem.getClbColumns()+1)).append(", ")
                    .append(i % (problem.getClbColumns()+1)).append(")\n");
            for (byte element : switchBoxes[i]) {
                sb.append(SwitchBoxTranslator.AS_STRINGS[element]).append(' ');
            }
            sb.deleteCharAt(sb.length()-1);
            sb.append('\n');
        }
        stream.write(sb.toString().getBytes());
    }
}
