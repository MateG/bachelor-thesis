package hr.fer.zemris.zavrad.algorithm;

import hr.fer.zemris.zavrad.algorithm.additional.CrossingsAlgorithm;
import hr.fer.zemris.zavrad.algorithm.additional.SegmentsAlgorithm;
import hr.fer.zemris.zavrad.algorithm.initial.StartingPointSolution;
import hr.fer.zemris.zavrad.algorithm.initial.StartingPointsAlgorithm;
import hr.fer.zemris.zavrad.algorithm.main.MainEvaluator;
import hr.fer.zemris.zavrad.algorithm.main.MainSolution;
import hr.fer.zemris.zavrad.algorithm.main.SASEGASA;
import hr.fer.zemris.zavrad.algorithm.solution.Dependency;
import hr.fer.zemris.zavrad.algorithm.solution.Evaluator;
import hr.fer.zemris.zavrad.algorithm.solution.Solution;
import hr.fer.zemris.zavrad.algorithm.translator.SignalTranslator;
import hr.fer.zemris.zavrad.problem.Problem;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hr.fer.zemris.zavrad.algorithm.translator.SignalTranslator.NO_SIGNAL;
import static hr.fer.zemris.zavrad.algorithm.translator.SignalTranslator.getInstance;

public class OptimizationAlgorithm implements Algorithm<Solution> {

    private Problem problem;

    private StartingPointsAlgorithm startingAlgorithm;

    private StartingPointSolution startingPoint;

    private SASEGASA mainAlgorithm;

    private Map<Dependency, Integer> unsatisfiedWeights = new HashMap<>();

    public OptimizationAlgorithm(Problem problem) {
        this.problem = problem;
        SignalTranslator.setProblem(problem);
        this.startingAlgorithm = new StartingPointsAlgorithm(
                problem, 5, 500_000, unsatisfiedWeights);
    }

    @Override
    public Solution solve() {
        MainSolution mainSolution;
        while (true) {
            System.out.println("Launching the starting point algorithm...");
            List<StartingPointSolution> startingPoints = startingAlgorithm
                    .solve();
            if (startingPoints.isEmpty()) {
                System.out.println("No starting point solutions found.");
                continue;
            }
            System.out.println("Done.\n");

            mainSolution = findFromStartingPoints(startingPoints);
            if (mainSolution == null) {
                System.out.println("No acceptable solutions found.\n");
                continue;
            }
            System.out.println("Done.\n");
            break;
        }

        System.out.print("Attaching CLBs and pins to appropriate segments...");
        MainEvaluator evaluator = mainAlgorithm.getEvaluator();
        CrossingsAlgorithm crossingsAlgorithm = new CrossingsAlgorithm(
                evaluator, mainSolution, evaluator.getFunctionPins(),
                evaluator.getFunctionPinSignals(), evaluator.getActiveBlocks());
        crossingsAlgorithm.solve();
        System.out.println("Done");

        Solution fullSolution = toFullSolution(startingPoint, mainSolution);
        Evaluator fullEvaluator = new Evaluator(problem);
        fullEvaluator.evaluate(fullSolution);

        System.out.println("Optimizing the number of used conductors...");
        SegmentsAlgorithm segmentOptimizer = new SegmentsAlgorithm(
                problem, fullSolution);
        fullSolution = segmentOptimizer.solve();
        System.out.println("Done.\n");

        return fullSolution;
    }

    private Solution toFullSolution(StartingPointSolution startingPoint,
            MainSolution mainSolution) {
        SignalTranslator translator = getInstance();
        short[] original = startingPoint.getIoPinIDs();
        short[] ioPinIDs = new short[original.length];
        System.arraycopy(original, 0, ioPinIDs, 0, ioPinIDs.length);
        for (int i = 0; i < ioPinIDs.length; i ++) {
            short signal = ioPinIDs[i];
            if (signal == NO_SIGNAL) continue;
            if (translator.isFunctionSignal(signal)) ioPinIDs[i] = NO_SIGNAL;
        }
        return new Solution(
                startingPoint.getClbIDs(),
                ioPinIDs,
                mainSolution.getIoPinCrossings(),
                mainSolution.getClbInputCrossings(),
                mainSolution.getClbOutputCrossings(),
                mainSolution.getSwitchBoxes()
        );
    }

    private MainSolution findFromStartingPoints(
            List<StartingPointSolution> startingPoints) {
        System.out.println("Launching the main algorithm iteratively...");
        int max = Math.min(startingPoints.size(), 3);
        for (int i = 0; i < max; i ++) {
            StartingPointSolution starting = startingPoints.get(i);
            mainAlgorithm = new SASEGASA(
                    problem, starting, 400, 20, unsatisfiedWeights);
            MainSolution mainSolution = mainAlgorithm.solve();

            if (mainSolution != null) {
                startingPoint = starting;
                return mainSolution;
            }
            System.out.println("Solution not acceptable");
        }
        return null;
    }
}
