package hr.fer.zemris.zavrad.algorithm.fpga;

/**
 * {@link FPGA} component which represents a conductive element of a
 * {@link Segment}.
 *
 * @author Mate Gašparini
 */
public class Conductor {

    /** The specified parent FPGA segment. */
    private final Segment parent;

    /** The specified index in the parent segment. */
    private final int index;

    /** The currently active signal; 1-indexed; 0 is no signal. */
    private short signal;

    /**
     * Constructor specifying the parent FPGA segment and the corresponding
     * index of this conductor.
     *
     * @param parent The specified parent FPGA segment.
     * @param index The specified index in the parent segment.
     */
    public Conductor(Segment parent, int index) {
        this.parent = parent;
        this.index = index;
    }

    /**
     * Returns the parent FPGA segment (specified during FPGA construction).
     *
     * @return The parent segment.
     */
    public Segment getParent() {
        return parent;
    }

    /**
     * Returns the specified index from the parent FPGA segment (specified
     * during FPGA construction).
     *
     * @return The index of this conductor.
     */
    public int getIndex() {
        return index;
    }

    /**
     * Returns the currently active signal. Zero denotes no signal.
     *
     * @return The signal value.
     */
    public short getSignal() {
        return signal;
    }

    /**
     * Sets the currently active signal to the given value. Zero denotes no
     * signal.
     *
     * @param signal The given value.
     */
    public void setSignal(short signal) {
        this.signal = signal;
    }
}
