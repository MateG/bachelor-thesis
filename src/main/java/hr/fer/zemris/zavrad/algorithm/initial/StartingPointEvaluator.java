package hr.fer.zemris.zavrad.algorithm.initial;

import hr.fer.zemris.zavrad.algorithm.solution.Dependency;
import hr.fer.zemris.zavrad.algorithm.translator.SignalTranslator;
import hr.fer.zemris.zavrad.problem.architecture.Architecture;

import java.util.Map;
import java.util.Objects;

import static hr.fer.zemris.zavrad.algorithm.translator.SignalTranslator.NO_CLB;
import static hr.fer.zemris.zavrad.algorithm.translator.SignalTranslator.NO_SIGNAL;

public class StartingPointEvaluator {

    private Point[] pins;

    private Point[] clbs;

    private SignalTranslator translator = SignalTranslator.getInstance();

    private int pinCost;

    private Map<Dependency, Integer> unsatisfiedWeights;

    public StartingPointEvaluator(Architecture architecture,
            Map<Dependency, Integer> unsatisfiedWeights) {
        this.pinCost = architecture.getClbRows()*architecture.getClbColumns();
        this.unsatisfiedWeights = unsatisfiedWeights;
        initPins(architecture);
        initCLBs(architecture);
    }

    public void evaluate(StartingPointSolution solution) {
        solution.acceptable = true;
        int cost = 0;
        short[] pinIDs = solution.ioPinIDs;
        short[] clbIDs = solution.clbIDs;

        for (int i = 0; i < clbIDs.length; i ++) {
            short id = clbIDs[i];
            if (id == NO_CLB) continue;

            short output = translator.getOutput(id);
            for (short input : translator.getInputs(id)) {
                int distance = Point.distance(clbs[i],
                        Objects.requireNonNull(translator.isInputSignal(input) ?
                                findPin(pinIDs, input) : findCLB(clbIDs, input))
                );
                cost += distance*getCostWeight(input, output);
            }
        }

        for (short i = 0; i < pinIDs.length; i ++) {
            short id = pinIDs[i];
            Short constraint = translator.getSignalConstraint(i);
            if (constraint != null && constraint != id) {
                cost += pinCost;
                solution.acceptable = false;
                continue;
            }
            if (id == NO_SIGNAL) continue;

            if (translator.isFunctionSignal(id)) {
                int distance = Point.distance(
                        pins[i], Objects.requireNonNull(findCLB(clbIDs, id)));
                cost += distance*getCostWeight(id, id);
            }
        }

        solution.cost = cost;
    }

    private void initPins(Architecture architecture) {
        int rows = architecture.getClbRows();
        int cols = architecture.getClbColumns();
        int pinsPerSegment = architecture.getPinsPerSegment();
        int pinCount = 2 * (rows + cols) * pinsPerSegment;
        pins = new Point[pinCount];

        int pinIndex = 0;
        for (int i = 0; i < cols; i ++) {
            for (int j = 0; j < pinsPerSegment; j ++) {
                pins[pinIndex++] = new Point(2*i+1, 0);
            }
        }
        for (int i = 0; i < rows; i ++) {
            for (int j = 0; j < pinsPerSegment; j ++) {
                pins[pinIndex++] = new Point(2*cols, 2*i+1);
            }
        }
        for (int i = cols-1; i >= 0; i --) {
            for (int j = 0; j < pinsPerSegment; j ++) {
                pins[pinIndex++] = new Point(2*i+1, 2*rows);
            }
        }
        for (int i = rows-1; i >= 0; i --) {
            for (int j = 0; j < pinsPerSegment; j ++) {
                pins[pinIndex++] = new Point(0, 2*i+1);
            }
        }
    }

    private void initCLBs(Architecture architecture) {
        int rows = architecture.getClbRows();
        int cols = architecture.getClbColumns();
        int clbCount = rows * cols;
        clbs = new Point[clbCount];

        int clbIndex = 0;
        for (int row = 0; row < rows; row ++) {
            for (int col = 0; col < cols; col ++) {
                clbs[clbIndex++] = new Point(2*col+1, 2*row+1);
            }
        }
    }

    private Point findPin(short[] pinIDs, short signal) {
        for (int i = 0; i < pinIDs.length; i ++) {
            if (pinIDs[i] == signal) return pins[i];
        }
        return null;
    }

    private Point findCLB(short[] clbIDs, short signal) {
        for (int i = 0; i < clbIDs.length; i ++) {
            if (clbIDs[i] == NO_CLB) continue;
            if (translator.getOutput(clbIDs[i]) == signal) return clbs[i];
        }
        return null;
    }

    private int getCostWeight(short from, short to) {
        Integer weight = unsatisfiedWeights.get(new Dependency(from, to));
        if (weight == null) return 1;
        return weight;
    }

    private static class Point {

        int x;

        int y;

        Point(int x, int y) {
            this.x = x;
            this.y = y;
        }

        static int distance(Point p1, Point p2) {
            return Math.abs(p1.x - p2.x) + Math.abs(p1.y - p2.y);
        }
    }
}
