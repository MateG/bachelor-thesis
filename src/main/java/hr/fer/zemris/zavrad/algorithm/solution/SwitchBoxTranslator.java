package hr.fer.zemris.zavrad.algorithm.solution;

public final class SwitchBoxTranslator {

    public static final byte NO_LINKS = 0;

    public static final byte UPPER_LEFT = 1;
    public static final byte UPPER_RIGHT = 2;
    public static final byte LOWER_LEFT = 3;
    public static final byte LOWER_RIGHT = 4;
    public static final byte HORIZONTAL = 5;
    public static final byte VERTICAL = 6;

    public static final byte DOUBLE_UPPER = 7;
    public static final byte DOUBLE_LOWER = 8;
    public static final byte DOUBLE_LEFT = 9;
    public static final byte DOUBLE_RIGHT = 10;
    public static final byte DOUBLE_UPPER_LEFT = 11;
    public static final byte DOUBLE_UPPER_RIGHT = 12;
    public static final byte CROSS = 13;

    public static final byte ALL_LINKS = 14;

    public static final int ELEMENT_TYPES = 15;

    public static final String[] AS_STRINGS = new String[] {
            "NO_LINKS",
            "UPPER_LEFT",
            "UPPER_RIGHT",
            "LOWER_LEFT",
            "LOWER_RIGHT",
            "HORIZONTAL",
            "VERTICAL",
            "DOUBLE_UPPER",
            "DOUBLE_LOWER",
            "DOUBLE_LEFT",
            "DOUBLE_RIGHT",
            "DOUBLE_UPPER_LEFT",
            "DOUBLE_UPPER_RIGHT",
            "CROSS",
            "ALL_LINKS"
    };

    public static final byte[] UPPER_DISCONNECTS = new byte[] {
            NO_LINKS,               // NO_LINKS
            NO_LINKS,               // UPPER_LEFT
            NO_LINKS,               // UPPER_RIGHT
            LOWER_LEFT,             // LOWER_LEFT
            LOWER_RIGHT,            // LOWER_RIGHT
            HORIZONTAL,             // HORIZONTAL
            NO_LINKS,               // VERTICAL
            HORIZONTAL,             // DOUBLE_UPPER
            DOUBLE_LOWER,           // DOUBLE_LOWER
            LOWER_LEFT,             // DOUBLE_LEFT
            LOWER_RIGHT,            // DOUBLE_RIGHT
            LOWER_RIGHT,            // DOUBLE_UPPER_LEFT
            LOWER_LEFT,             // DOUBLE_UPPER_RIGHT
            HORIZONTAL,             // CROSS
            DOUBLE_LOWER            // ALL_LINKS
    };

    public static final byte[] LOWER_DISCONNECTS = new byte[] {
            NO_LINKS,               // NO_LINKS
            UPPER_LEFT,             // UPPER_LEFT
            UPPER_RIGHT,            // UPPER_RIGHT
            NO_LINKS,               // LOWER_LEFT
            NO_LINKS,               // LOWER_RIGHT
            HORIZONTAL,             // HORIZONTAL
            NO_LINKS,               // VERTICAL
            DOUBLE_UPPER,           // DOUBLE_UPPER
            HORIZONTAL,             // DOUBLE_LOWER
            UPPER_LEFT,             // DOUBLE_LEFT
            UPPER_RIGHT,            // DOUBLE_RIGHT
            UPPER_LEFT,             // DOUBLE_UPPER_LEFT
            UPPER_RIGHT,            // DOUBLE_UPPER_RIGHT
            HORIZONTAL,             // CROSS
            DOUBLE_UPPER            // ALL_LINKS
    };

    public static final byte[] LEFT_DISCONNECTS = new byte[] {
            NO_LINKS,               // NO_LINKS
            NO_LINKS,               // UPPER_LEFT
            UPPER_RIGHT,            // UPPER_RIGHT
            NO_LINKS,               // LOWER_LEFT
            LOWER_RIGHT,            // LOWER_RIGHT
            NO_LINKS,               // HORIZONTAL
            VERTICAL,               // VERTICAL
            UPPER_RIGHT,            // DOUBLE_UPPER
            LOWER_RIGHT,            // DOUBLE_LOWER
            VERTICAL,               // DOUBLE_LEFT
            DOUBLE_RIGHT,           // DOUBLE_RIGHT
            LOWER_RIGHT,            // DOUBLE_UPPER_LEFT
            UPPER_RIGHT,            // DOUBLE_UPPER_RIGHT
            VERTICAL,               // CROSS
            DOUBLE_RIGHT            // ALL_LINKS
    };

    public static final byte[] RIGHT_DISCONNECTS = new byte[] {
            NO_LINKS,               // NO_LINKS
            UPPER_LEFT,             // UPPER_LEFT
            NO_LINKS,               // UPPER_RIGHT
            LOWER_LEFT,             // LOWER_LEFT
            NO_LINKS,               // LOWER_RIGHT
            NO_LINKS,               // HORIZONTAL
            VERTICAL,               // VERTICAL
            UPPER_LEFT,             // DOUBLE_UPPER
            LOWER_LEFT,             // DOUBLE_LOWER
            DOUBLE_LEFT,            // DOUBLE_LEFT
            VERTICAL,               // DOUBLE_RIGHT
            UPPER_LEFT,             // DOUBLE_UPPER_LEFT
            LOWER_LEFT,             // DOUBLE_UPPER_RIGHT
            VERTICAL,               // CROSS
            DOUBLE_LEFT             // ALL_LINKS
    };

    /**
     * Default private constructor.
     */
    private SwitchBoxTranslator() {
    }

    public static boolean isHorizontal(byte element) {
        return element == HORIZONTAL
                || element == DOUBLE_LOWER
                || element == DOUBLE_UPPER
                || element == CROSS
                || element == ALL_LINKS;
    }

    public static boolean isVertical(byte element) {
        return element == VERTICAL
                || element == DOUBLE_LEFT
                || element == DOUBLE_RIGHT
                || element == CROSS
                || element == ALL_LINKS;
    }

    public static boolean isLowerLeft(byte element) {
        return element == LOWER_LEFT
                || element == DOUBLE_LOWER
                || element == DOUBLE_LEFT
                || element == DOUBLE_UPPER_RIGHT
                || element == ALL_LINKS;
    }

    public static boolean isLowerRight(byte element) {
        return element == LOWER_RIGHT
                || element == DOUBLE_LOWER
                || element == DOUBLE_RIGHT
                || element == DOUBLE_UPPER_LEFT
                || element == ALL_LINKS;
    }

    public static boolean isUpperLeft(byte element) {
        return element == UPPER_LEFT
                || element == DOUBLE_UPPER
                || element == DOUBLE_LEFT
                || element == DOUBLE_UPPER_LEFT
                || element == ALL_LINKS;
    }

    public static boolean isUpperRight(byte element) {
        return element == UPPER_RIGHT
                || element == DOUBLE_UPPER
                || element == DOUBLE_RIGHT
                || element == DOUBLE_UPPER_RIGHT
                || element == ALL_LINKS;
    }
}
