package hr.fer.zemris.zavrad.algorithm.translator;

/**
 * Thrown to indicate that the {@link SignalTranslator} has failed to translate
 * some signal specified in the problem.
 *
 * @author Mate Gašparini
 */
public class SignalTranslatorException extends RuntimeException {

    /**
     * Constructs an {@code SignalTranslatorException} with no detail message.
     */
    public SignalTranslatorException() {
    }

    /**
     * Constructs an {@code SignalTranslatorException} with the specified detail
     * message.
     *
     * @param message The specified detail message.
     */
    public SignalTranslatorException(String message) {
        super(message);
    }

    /**
     * Constructs an {@code SignalTranslatorException} with the specified detail
     * message and cause.
     *
     * @param message The specified detail message.
     * @param cause The specified cause.
     */
    public SignalTranslatorException(String message, Throwable cause) {
        super(message, cause);
    }
}
