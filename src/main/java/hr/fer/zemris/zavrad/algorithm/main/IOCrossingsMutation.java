package hr.fer.zemris.zavrad.algorithm.main;

import hr.fer.zemris.zavrad.random.RandomSingleton;

import java.util.Random;

public class IOCrossingsMutation {

    private Random random = RandomSingleton.getInstance();

    private short[] pinIndexes;

    private int conductors;

    public IOCrossingsMutation(short[] pinIndexes, int conductors) {
        this.pinIndexes = pinIndexes;
        this.conductors = conductors;
    }

    public void mutate(MainSolution solution) {
        byte[] crossings = solution.getIoPinCrossings();
        short index = pinIndexes[random.nextInt(pinIndexes.length)];
        crossings[index] = (byte) (1 + random.nextInt(conductors));
    }
}
