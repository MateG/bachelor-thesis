package hr.fer.zemris.zavrad.algorithm.fpga;

import hr.fer.zemris.zavrad.problem.architecture.Architecture;

/**
 * <i>Factory</i> class which produces an {@link FPGA} object compliant with
 * some specified {@link Architecture}.
 *
 * @author Mate Gašparini
 */
public class FPGAFactory {

    /** The specified architecture. */
    private Architecture architecture;

    /** The constructed FPGA pin array. */
    private Pin[] pins;

    /** The constructed FPGA switch box matrix. */
    private SwitchBox[][] switchBoxes;

    /** The constructed FPGA CLB matrix. */
    private Block[][] blocks;

    /** Indicates the next pin to connect (stored to reduce stack usage). */
    private int pin;

    /**
     * Constructor specifying the FPGA architecture.
     *
     * @param architecture The specified architecture.
     */
    public FPGAFactory(Architecture architecture) {
        this.architecture = architecture;
    }

    /**
     * Constructs an {@link FPGA} compliant with the specified architecture and
     * links all its components accordingly.
     *
     * @return The constructed {@link FPGA}.
     */
    public FPGA createFPGA() {
        int rows = architecture.getClbRows();
        int columns = architecture.getClbColumns();
        blocks = new Block[rows][columns];
        createSwitchBoxes(rows+1, columns+1);
        connectSwitchBoxes(rows, columns);

        int outerSegments = 2*(rows + columns);
        int pinsPerSegment = architecture.getPinsPerSegment();
        createPins(outerSegments * pinsPerSegment);
        connectPins(rows, columns);

        return new FPGA(pins, switchBoxes, blocks);
    }

    /**
     * Instantiates the switch boxes and stores their references.
     *
     * @param rows The specified switch box row count.
     * @param columns The specified switch box column count.
     */
    private void createSwitchBoxes(int rows, int columns) {
        switchBoxes = new SwitchBox[rows][columns];
        for (int row = 0; row < rows; row ++) {
            for (int col = 0; col < columns; col ++) {
                switchBoxes[row][col] = new SwitchBox();
            }
        }
    }

    /**
     * Connects the switch boxes and CLBs with {@link Segment} components.
     *
     * @param rows The specified CLB row count.
     * @param columns The specified CLB column count.
     */
    private void connectSwitchBoxes(int rows, int columns) {
        int conductorCount = architecture.getConductorsPerSegment();
        connectHorizontally(rows, columns, conductorCount);
        connectVertically(rows, columns, conductorCount);
        connectCLBs(rows, columns);
    }

    /**
     * Connects the switch boxes with horizontal {@link Segment}s.
     *
     * @param rows The specified CLB row count.
     * @param columns The specified CLB column count.
     * @param conductorCount The specified conductor per segment count.
     */
    private void connectHorizontally(int rows, int columns,
            int conductorCount) {
        for (int row = 0; row < rows + 1; row ++) {
            for (int col = 0; col < columns; col ++) {
                SwitchBox current = switchBoxes[row][col];
                SwitchBox next = switchBoxes[row][col+1];
                Segment between = new Segment(current, next, conductorCount);
                current.right = between;
                next.left = between;
            }
        }
    }

    /**
     * Connects the switch boxes with {@link VerticalSegment}s.
     *
     * @param rows The specified CLB row count.
     * @param columns The specified CLB column count.
     * @param conductorCount The specified conductor per segment count.
     */
    private void connectVertically(int rows, int columns, int conductorCount) {
        for (int col = 0; col < columns + 1; col ++) {
            boolean hasBlock = col < columns;
            for (int row = 0; row < rows; row ++) {
                SwitchBox current = switchBoxes[row][col];
                SwitchBox next = switchBoxes[row+1][col];
                VerticalSegment between = new VerticalSegment(
                        current, next, conductorCount
                );
                if (hasBlock) {
                    Block block = new Block();
                    between.block = block;
                    blocks[row][col] = block;
                }
                current.below = between;
                next.above = between;
            }
        }
    }

    /**
     * Connect CLBs with their output {@link VerticalSegment}s.
     *
     * @param rows The specified CLB row count.
     * @param columns The specified CLB column count.
     */
    private void connectCLBs(int rows, int columns) {
        for (int row = 0; row < rows; row ++) {
            for (int col = 0; col < columns; col ++) {
                VerticalSegment input = (VerticalSegment)
                        switchBoxes[row][col].getBelow();
                Block block = input.getBlock();
                block.input = input;
                block.output = switchBoxes[row][col+1].getBelow();
            }
        }
    }

    /**
     * Instantiates the I/O pins and stores their references.
     *
     * @param pinCount The specified pin count.
     */
    private void createPins(int pinCount) {
        pins = new Pin[pinCount];
        for (int i = 0; i < pinCount; i ++) {
            pins[i] = new Pin();
        }
    }

    /**
     * Connects the I/O pins with {@link Segment} components.
     *
     * @param rows The specified CLB row count.
     * @param columns The specified CLB column count.
     */
    private void connectPins(int rows, int columns) {
        pin = 0;

        // Connect top IO pins and segments.
        for (int col = 0; col < columns; col ++) {
            connectCurrentPins(switchBoxes[0][col].getRight());
        }

        // Connect right IO pins and segments.
        for (int row = 0; row < rows; row ++) {
            connectCurrentPins(switchBoxes[row][columns].getBelow());
        }

        // Connect bottom IO pins and segments.
        for (int col = columns; col > 0; col --) {
            connectCurrentPins(switchBoxes[rows][col].getLeft());
        }

        // Connect right IO pins and segments.
        for (int row = rows; row > 0; row --) {
            connectCurrentPins(switchBoxes[row][0].getAbove());
        }
    }

    /**
     * Connects the current segment pins with the given {@link Segment}.
     *
     * @param segment The given segment component.
     */
    private void connectCurrentPins(Segment segment) {
        int pinsPerSegment = architecture.getPinsPerSegment();
        for (int i = 0; i < pinsPerSegment; i ++, pin ++) {
            pins[pin].segment = segment;
        }
    }
}
