package hr.fer.zemris.zavrad.algorithm.main;

import hr.fer.zemris.zavrad.random.RandomSingleton;

import java.util.Random;

import static hr.fer.zemris.zavrad.algorithm.solution.SwitchBoxTranslator.ELEMENT_TYPES;
import static hr.fer.zemris.zavrad.algorithm.solution.SwitchBoxTranslator.NO_LINKS;

public class SwitchBoxMutation {

    private Random random = RandomSingleton.getInstance();

    public void mutate(MainSolution solution) {
        byte[][] switchBoxes = solution.getSwitchBoxes();
        byte[] matrix = switchBoxes[random.nextInt(switchBoxes.length)];
        matrix[random.nextInt(matrix.length)] = (byte) (random.nextBoolean() ?
                NO_LINKS : random.nextInt(ELEMENT_TYPES));
    }
}
