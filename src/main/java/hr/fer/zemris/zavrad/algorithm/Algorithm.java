package hr.fer.zemris.zavrad.algorithm;

public interface Algorithm<T> {

    T solve();
}
