package hr.fer.zemris.zavrad.algorithm.search;

import hr.fer.zemris.zavrad.algorithm.fpga.Segment;
import hr.fer.zemris.zavrad.algorithm.fpga.VerticalSegment;
import hr.fer.zemris.zavrad.algorithm.translator.SignalTranslator;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

import static hr.fer.zemris.zavrad.algorithm.translator.SignalTranslator.NO_CLB;

public class BFS {

    private Queue<Node> open = new LinkedList<>();

    private Set<Segment> closed = new HashSet<>();

    public int minDistance(Segment start, short signal) {
        open.clear();
        Node node = new Node(start, 0);
        open.add(node);
        closed.clear();
        while (!open.isEmpty()) {
            node = open.poll();
            Segment current = node.segment;
            if (current.containsSignal(signal)) break;
            if (blockOutputMatches(current, signal)) break;

            for (Segment successor : SearchUtil.successors(current)) {
                if (closed.contains(successor)) continue;
                open.offer(new Node(successor, node.distance + 1));
            }

            closed.add(current);
        }
        return node.distance;
    }

    private boolean blockOutputMatches(Segment segment, short signal) {
        if (segment instanceof VerticalSegment) {
            segment = segment.getFrom().getLeft();
            if (segment == null) return false;
            segment = segment.getFrom().getBelow();
            short id = ((VerticalSegment) segment).getBlock().getId();
            if (id == NO_CLB) return false;
            if (SignalTranslator.getInstance().getOutput(id) == signal) {
                return true;
            }
        }
        return false;
    }

    private static class Node {

        Segment segment;

        int distance;

        Node(Segment segment, int distance) {
            this.segment = segment;
            this.distance = distance;
        }
    }
}
