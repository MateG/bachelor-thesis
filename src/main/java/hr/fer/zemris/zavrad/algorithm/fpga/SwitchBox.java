package hr.fer.zemris.zavrad.algorithm.fpga;

/**
 * {@link FPGA} component which connects (at most four) {@link Segment}s.
 *
 * @author Mate Gašparini
 */
public class SwitchBox {

    /** The specified right segment. */
    Segment right;

    /** The specified lower segment. */
    Segment below;

    /** The specified left segment. */
    Segment left;

    /** The specified upper segment. */
    Segment above;

    /** The current link matrix elements. */
    private byte[] matrix;

    /**
     * Returns the right segment reference, or {@code null} if this switch box
     * does not have a right side neighbor (specified during FPGA construction).
     *
     * @return The right segment.
     */
    public Segment getRight() {
        return right;
    }

    /**
     * Returns the lower segment reference, or {@code null} if this switch box
     * does not have a lower side neighbor (specified during FPGA construction).
     *
     * @return The lower segment.
     */
    public Segment getBelow() {
        return below;
    }

    /**
     * Returns the left segment reference, or {@code null} if this switch box
     * does not have a left side neighbor (specified during FPGA construction).
     *
     * @return The left segment.
     */
    public Segment getLeft() {
        return left;
    }

    /**
     * Returns the upper segment reference, or {@code null} if this switch box
     * does not have a upper side neighbor (specified during FPGA construction).
     *
     * @return The upper segment.
     */
    public Segment getAbove() {
        return above;
    }

    /**
     * Returns the current link matrix elements.
     *
     * @return The current link matrix.
     */
    public byte[] getMatrix() {
        return matrix;
    }

    /**
     * Sets the current link matrix elements to the given matrix elements.
     *
     * @param matrix The given matrix.
     */
    public void setMatrix(byte[] matrix) {
        this.matrix = matrix;
    }
}
