package hr.fer.zemris.zavrad.algorithm.main;

import hr.fer.zemris.zavrad.random.RandomSingleton;

import java.util.Random;

public class MainMutation {

    private Random random = RandomSingleton.getInstance();

    private SwitchBoxMutation switchBox = new SwitchBoxMutation();

    private IOCrossingsMutation ioCrossings;

    private CLBOutputCrossingsMutation clbOutputCrossings;

    public MainMutation(short[] pinIndexes, int conductors) {
        ioCrossings = new IOCrossingsMutation(pinIndexes, conductors);
        clbOutputCrossings = new CLBOutputCrossingsMutation(conductors);
    }

    public void mutate(MainSolution solution) {
        double value = random.nextDouble();
        if (value < 0.9) {
            switchBox.mutate(solution);
        } else if (value < 0.95) {
            ioCrossings.mutate(solution);
        } else {
            clbOutputCrossings.mutate(solution);
        }
    }
}
