package hr.fer.zemris.zavrad.algorithm.main;

import hr.fer.zemris.zavrad.problem.Problem;
import hr.fer.zemris.zavrad.random.RandomSingleton;

import java.util.Random;

public class MainSolutionGenerator {

    private Random random = RandomSingleton.getInstance();

    private short[] ioPinIDs;

    private int usedClbCount;

    private int boxCount;

    private int pinCount;

    private int clbInputs;

    private int conductors;

    public MainSolutionGenerator(Problem problem, short[] ioPinIDs) {
        this.ioPinIDs = ioPinIDs;
        boxCount = (problem.getClbRows() + 1) * (problem.getClbColumns() + 1);
        usedClbCount = problem.getBlocks().size();
        pinCount = 2 * (problem.getClbRows() + problem.getClbColumns())
                * problem.getPinsPerSegment();
        clbInputs = problem.getClbInputs();
        conductors = problem.getConductorsPerSegment();
    }

    public MainSolution emptySolution() {
        return new MainSolution(
                new byte[pinCount],
                new byte[usedClbCount][clbInputs],
                new byte[usedClbCount],
                new byte[boxCount][conductors]
        );
    }

    public MainSolution randomSolution() {
        return new MainSolution(
                randomIoPinCrossings(ioPinIDs),
                randomClbInputCrossings(),
                randomClbOutputCrossings(),
                randomSwitchBoxes()
        );
    }

    private byte[] randomIoPinCrossings(short[] ioPinIDs) {
        byte[] ioPinCrossings = new byte[pinCount];
        for (int i = 0; i < ioPinCrossings.length; i ++) {
            if (ioPinIDs[i] != 0) {
                ioPinCrossings[i] = (byte) (1 + random.nextInt(conductors));
            } else {
                ioPinCrossings[i] = 0;
            }
        }
        return ioPinCrossings;
    }

    private byte[][] randomClbInputCrossings() {
        byte[][] clbInputCrossings = new byte[usedClbCount][clbInputs];
        for (byte[] inputs : clbInputCrossings) {
            for (int input = 0; input < clbInputs; input ++) {
                inputs[input] = (byte) random.nextInt(conductors);
            }
        }
        return clbInputCrossings;
    }

    private byte[] randomClbOutputCrossings() {
        byte[] clbOutputCrossings = new byte[usedClbCount];
        for (int i = 0; i < clbOutputCrossings.length; i ++) {
            clbOutputCrossings[i] = (byte) random.nextInt(conductors);
        }
        return clbOutputCrossings;
    }

    private byte[][] randomSwitchBoxes() {
        byte[][] boxes = new byte[boxCount][conductors];
        for (byte[] matrix : boxes) {
            for (int i = 0; i < matrix.length; i ++) {
                matrix[i] = 0;
            }
        }
        return boxes;
    }
}
