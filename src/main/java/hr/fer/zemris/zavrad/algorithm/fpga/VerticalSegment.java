package hr.fer.zemris.zavrad.algorithm.fpga;

/**
 * Special case of the {@link Segment} component which contains an additional
 * reference to a single {@link Block}.
 *
 * @author Mate Gašparini
 */
public class VerticalSegment extends Segment {

    /** The corresponding CLB reference. */
    Block block;

    /**
     * Constructor specifying the switch box references and conductor count.
     *
     * @param from The specified (upper of left) switch box.
     * @param to The specified (lower or right) switch box.
     * @param conductorCount The specified conductor count.
     */
    public VerticalSegment(SwitchBox from, SwitchBox to, int conductorCount) {
        super(from, to, conductorCount);
    }

    /**
     * Returns the corresponding CLB reference (specified during FPGA
     * construction).
     *
     * @return The corresponding CLB.
     */
    public Block getBlock() {
        return block;
    }
}
