package hr.fer.zemris.zavrad.algorithm.solution;

import java.util.Objects;

public class Dependency {

    private final Short start;

    private final Short end;

    public Dependency(Short start, Short end) {
        this.start = start;
        this.end = end;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Dependency that = (Dependency) o;
        return start.equals(that.start) &&
                end.equals(that.end);
    }

    @Override
    public int hashCode() {
        return Objects.hash(start, end);
    }

    @Override
    public String toString() {
        return "(" + start + ", " + end + ')';
    }
}
