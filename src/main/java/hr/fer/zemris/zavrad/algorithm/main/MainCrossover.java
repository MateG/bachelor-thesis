package hr.fer.zemris.zavrad.algorithm.main;

import hr.fer.zemris.zavrad.random.RandomSingleton;

import java.util.Arrays;
import java.util.Random;

public class MainCrossover {

    private Random random = RandomSingleton.getInstance();

    public MainSolution cross(MainSolution first, MainSolution second) {
        return new MainSolution(
                ioPinCrossings(first, second),
                clbInputCrossings(first, second),
                clbOutputCrossings(first, second),
                switchBoxes(first, second)
        );
    }

    private byte[] ioPinCrossings(MainSolution first, MainSolution second) {
        byte[] original = random.nextBoolean() ?
                first.getIoPinCrossings() : second.getIoPinCrossings();
        return Arrays.copyOf(original, original.length);
    }

    private byte[][] clbInputCrossings(MainSolution first, MainSolution second) {
        byte[][] original = random.nextBoolean() ?
                first.getClbInputCrossings() : second.getClbInputCrossings();
        byte[][] copy = new byte[original.length][];
        for (int i = 0; i < copy.length; i ++) {
            copy[i] = Arrays.copyOf(original[i], original[i].length);
        }
        return copy;
    }

    private byte[] clbOutputCrossings(MainSolution first, MainSolution second) {
        byte[] original = random.nextBoolean() ?
                first.getClbOutputCrossings() : second.getClbOutputCrossings();
        return Arrays.copyOf(original, original.length);
    }

    private byte[][] switchBoxes(MainSolution first, MainSolution second) {
        byte[][] original = random.nextBoolean() ?
                first.getSwitchBoxes() : second.getSwitchBoxes();
        byte[][] copy = new byte[original.length][];
        for (int i = 0; i < copy.length; i ++) {
            copy[i] = Arrays.copyOf(original[i], original[i].length);
        }
        return copy;
    }
}
