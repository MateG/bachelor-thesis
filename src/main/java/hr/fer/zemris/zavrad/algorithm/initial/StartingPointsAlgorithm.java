package hr.fer.zemris.zavrad.algorithm.initial;

import hr.fer.zemris.zavrad.algorithm.Algorithm;
import hr.fer.zemris.zavrad.algorithm.annealing.GeometricTempSchedule;
import hr.fer.zemris.zavrad.algorithm.annealing.TempSchedule;
import hr.fer.zemris.zavrad.algorithm.solution.Dependency;
import hr.fer.zemris.zavrad.problem.Problem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class StartingPointsAlgorithm implements Algorithm<List<StartingPointSolution>> {

    private static final int OUTER_LOOP = 10;

    private int populationSize;

    private int maxIterations;

    private StartingPointSolutionGenerator generator;

    private StartingPointEvaluator evaluator;

    private StartingPointSelection selection;

    private StartingPointMutation mutation;

    private List<StartingPointSolution> population;

    private List<StartingPointSolution> solutions;

    public StartingPointsAlgorithm(Problem problem, int populationSize,
            int maxIterations, Map<Dependency, Integer> unsatisfiedWeights) {
        this.populationSize = populationSize;
        this.maxIterations = maxIterations;

        generator = new StartingPointSolutionGenerator(problem);
        evaluator = new StartingPointEvaluator(problem, unsatisfiedWeights);
        selection = new StartingPointSelection();
        mutation = new StartingPointMutation();

        solutions = new ArrayList<>();
    }

    @Override
    public List<StartingPointSolution> solve() {
        initPopulation();
        evaluateSolutions(population);
        Collections.sort(population);
        StartingPointSolution allTimeBest = null;

        TempSchedule tempSchedule = new GeometricTempSchedule(8.0, 1.0,
                maxIterations / OUTER_LOOP, OUTER_LOOP);
        int outerLimit = tempSchedule.getOuterLoopCounter();
        int innerLimit = tempSchedule.getInnerLoopCounter();
        int gen = 1;
        for (int outer = 0; outer < outerLimit; outer ++) {
            double temperature = tempSchedule.getNextTemperature();
            for (int i = 0; i < innerLimit; i ++, gen ++) {
                StartingPointSolution worst = population.get(
                        population.size() - 1);
                selection.select(population).overwrite(worst);
                for (int j = 0; j < temperature; j++) {
                    mutation.mutate(worst);
                }
                evaluator.evaluate(worst);

                Collections.sort(population);
                StartingPointSolution best = population.get(0);
                if (allTimeBest == null || best.compareTo(allTimeBest) < 0) {
                    allTimeBest = best;
                    solutions.add(best);
                    StartingPointSolution copy = generator.emptySolution();
                    best.overwrite(copy);
                    System.out.println("[Gen " + gen + "] " + best.cost);
                }
            }
        }

        Collections.sort(solutions);
        return solutions;
    }

    private void initPopulation() {
        population = new ArrayList<>(populationSize);
        for (int i = 0; i < populationSize; i ++) {
            population.add(generator.randomSolution());
        }
    }

    private void evaluateSolutions(Iterable<StartingPointSolution> solutions) {
        for (StartingPointSolution solution : solutions) {
            evaluator.evaluate(solution);
        }
    }
}
