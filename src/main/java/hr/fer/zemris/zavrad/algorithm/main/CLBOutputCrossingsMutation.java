package hr.fer.zemris.zavrad.algorithm.main;

import hr.fer.zemris.zavrad.random.RandomSingleton;

import java.util.Random;

public class CLBOutputCrossingsMutation {

    private Random random = RandomSingleton.getInstance();

    private int conductors;

    public CLBOutputCrossingsMutation(int conductors) {
        this.conductors = conductors;
    }

    public void mutate(MainSolution solution) {
        byte[] all = solution.getClbOutputCrossings();
        all[random.nextInt(all.length)] = (byte) random.nextInt(conductors);
    }
}
