package hr.fer.zemris.zavrad.algorithm.main;

import java.util.Arrays;
import java.util.Comparator;

public class MainSolution implements Comparable<MainSolution> {

    private static final Comparator<MainSolution> COMPARATOR = Comparator
            .comparingInt(MainSolution::getCost);

    byte[] ioPinCrossings;

    byte[][] clbInputCrossings;

    byte[] clbOutputCrossings;

    byte[][] switchBoxes;

    private int signalClashes;

    private int distanceCost;

    private int conductorsUsed;

    public MainSolution(byte[] ioPinCrossings, byte[][] clbInputCrossings,
            byte[] clbOutputCrossings, byte[][] switchBoxes) {
        this.ioPinCrossings = ioPinCrossings;
        this.clbInputCrossings = clbInputCrossings;
        this.clbOutputCrossings = clbOutputCrossings;
        this.switchBoxes = switchBoxes;
    }

    public byte[] getIoPinCrossings() {
        return ioPinCrossings;
    }

    public byte[][] getClbInputCrossings() {
        return clbInputCrossings;
    }

    public byte[] getClbOutputCrossings() {
        return clbOutputCrossings;
    }

    public byte[][] getSwitchBoxes() {
        return switchBoxes;
    }

    public int getSignalClashes() {
        return signalClashes;
    }

    public void setSignalClashes(int signalClashes) {
        this.signalClashes = signalClashes;
    }

    public int getDistanceCost() {
        return distanceCost;
    }

    public void setDistanceCost(int distanceCost) {
        this.distanceCost = distanceCost;
    }

    public int getConductorsUsed() {
        return conductorsUsed;
    }

    public void setConductorsUsed(int conductorsUsed) {
        this.conductorsUsed = conductorsUsed;
    }

    public boolean isAcceptable() {
        return signalClashes == 0 && distanceCost == 0;
    }

    public int getCost() {
        return 2*signalClashes + distanceCost;
    }

    public void overwrite(MainSolution to) {
        System.arraycopy(this.ioPinCrossings, 0, to.ioPinCrossings, 0,
                ioPinCrossings.length);
        System.arraycopy(this.clbOutputCrossings, 0, to.clbOutputCrossings, 0,
                clbOutputCrossings.length);
        matrixCopy(this.switchBoxes, to.switchBoxes);

    }

    @Override
    public int compareTo(MainSolution o) {
        return COMPARATOR.compare(this, o);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MainSolution that = (MainSolution) o;
        return Arrays.equals(ioPinCrossings, that.ioPinCrossings) &&
                Arrays.equals(clbInputCrossings, that.clbInputCrossings) &&
                Arrays.equals(clbOutputCrossings, that.clbOutputCrossings) &&
                Arrays.equals(switchBoxes, that.switchBoxes);
    }

    @Override
    public int hashCode() {
        int result = Arrays.hashCode(ioPinCrossings);
        result = 31 * result + Arrays.hashCode(clbInputCrossings);
        result = 31 * result + Arrays.hashCode(clbOutputCrossings);
        result = 31 * result + Arrays.hashCode(switchBoxes);
        return result;
    }

    private void matrixCopy(byte[][] from, byte[][] to) {
        for (int i = 0; i < to.length; i ++) {
            System.arraycopy(from[i], 0, to[i], 0, to[i].length);
        }
    }
}
