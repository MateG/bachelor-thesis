package hr.fer.zemris.zavrad.algorithm.translator;

import hr.fer.zemris.zavrad.problem.Problem;
import hr.fer.zemris.zavrad.problem.clb.CLB;

import java.util.*;

public class SignalTranslator {

    public static final short NO_SIGNAL = 0;

    public static final short NO_CLB = 0;

    private static SignalTranslator instance;

    private Map<Short, String> signalMap = new HashMap<>();

    private Map<String, Short> signalMapInverse = new HashMap<>();

    private Map<Short, Block> blockMap = new HashMap<>();

    private Map<Short, Short> pinMap = new HashMap<>();

    private Map<Short, Short> pinMapInverse = new HashMap<>();

    private Set<Short> inputSignals = new HashSet<>();

    private Set<Short> functionSignals = new HashSet<>();

    public static SignalTranslator getInstance() {
        return instance;
    }

    public static void setProblem(Problem problem) {
        SignalTranslator.instance = new SignalTranslator(problem);
    }

    private SignalTranslator(Problem problem) {
        Objects.requireNonNull(problem, "Problem must not be null");
        initSignalMap(problem);
        initBlockMap(problem);
        initPinMap(problem);
    }

    public Set<Short> getInputSignals() {
        return inputSignals;
    }

    public int getInputSignalCount() {
        return inputSignals.size();
    }

    public String getSignalName(Short signal) {
        return signalMap.get(signal);
    }

    public short getOutput(Short id) {
        return blockMap.get(id).output;
    }

    public short[] getInputs(Short id) {
        return blockMap.get(id).inputs;
    }

    public Short getPinIndexConstraint(Short signal) {
        return pinMap.get(signal);
    }

    public boolean noPinConstraints() {
        return pinMap.isEmpty();
    }

    public Short getSignalConstraint(Short pinIndex) {
        return pinMapInverse.get(pinIndex);
    }

    public Set<Short> getFunctionSignals() {
        return functionSignals;
    }

    public boolean isFunctionSignal(Short signal) {
        return functionSignals.contains(signal);
    }

    public boolean isInputSignal(Short signal) {
        return inputSignals.contains(signal);
    }

    private void initSignalMap(Problem problem) {
        Set<String> inputLabels = SignalUtil.inputLabels(problem);
        inputSignals = new HashSet<>(inputLabels.size());

        short i = 1;
        for (String label : inputLabels) {
            inputSignals.add(i);
            signalMap.put(i, label);
            signalMapInverse.put(label, i++);
        }
        for (CLB clb : problem.getBlocks()) {
            signalMapInverse.put(clb.getId(), i);
            if (clb.getFunctionLabel() != null) {
                signalMap.put(i, clb.getFunctionLabel());
                signalMapInverse.put(clb.getFunctionLabel(), i);
                functionSignals.add(i);
            }
            i++;
        }
    }

    private void initBlockMap(Problem problem) {
        List<CLB> blocks = problem.getBlocks();
        for (short i = 1; i <= blocks.size(); i ++) {
            CLB clb = blocks.get(i-1);
            short output = signalMapInverse.get(clb.getId());
            short[] inputs = translateInputs(clb.getInputs());
            blockMap.put(i, new Block(output, inputs));
        }
    }

    private short[] translateInputs(String[] inputLabels) {
        short[] inputs = new short[inputLabels.length];
        for (int i = 0; i < inputs.length; i ++) {
            inputs[i] = signalMapInverse.get(inputLabels[i]);
        }
        return inputs;
    }

    private void initPinMap(Problem problem) {
        int maxPinIndex = problem.getPinsPerSegment()
                * 2 * (problem.getClbRows() + problem.getClbColumns());
        for (Map.Entry<String, String> entry : problem.getPinMap().entrySet()) {
            short pinIndex;
            try {
                pinIndex = Short.parseShort(entry.getValue());
                if (pinIndex < 0 || pinIndex > maxPinIndex) {
                    throw new SignalTranslatorException(
                            "Expected pin index from [" + 0 + ", "
                            + maxPinIndex + "], but was " + pinIndex);
                }
            } catch (NumberFormatException ex) {
                throw new SignalTranslatorException(
                        "Invalid pin index: " + entry.getValue());
            }

            String label = entry.getKey();
            Short signal = signalMapInverse.get(label);
            if (signal == null) {
                throw new SignalTranslatorException(
                        "Unknown signal label: " + label);
            }
            pinMap.put(signal, pinIndex);
            pinMapInverse.put(pinIndex, signal);
        }
    }

    private static class Block {

        short output;

        short[] inputs;

        Block(short output, short[] inputs) {
            this.output = output;
            this.inputs = inputs;
        }
    }
}
