package hr.fer.zemris.zavrad.algorithm.annealing;

public class GeometricTempSchedule implements TempSchedule {

    private double alpha;

    private double tInitial;

    private double tCurrent;

    private final int innerLimit;

    private final int outerLimit;

    public GeometricTempSchedule(double tInitial, double tFinal,
            int innerLimit, int outerLimit) {
        this.tInitial = tInitial;
        this.tCurrent = tInitial;
        this.innerLimit = innerLimit;
        this.outerLimit = outerLimit;
        calculateAlpha(tFinal);
    }

    @Override
    public double getNextTemperature() {
        return tCurrent *= alpha;
    }

    @Override
    public int getInnerLoopCounter() {
        return innerLimit;
    }

    @Override
    public int getOuterLoopCounter() {
        return outerLimit;
    }

    private void calculateAlpha(double tFinal) {
        if (tFinal == 0.0) tFinal = 1.0;
        alpha = Math.pow(tFinal / tInitial, 1.0 / (outerLimit - 1));
    }
}
