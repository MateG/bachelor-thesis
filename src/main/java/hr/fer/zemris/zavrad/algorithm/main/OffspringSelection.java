package hr.fer.zemris.zavrad.algorithm.main;

import hr.fer.zemris.zavrad.random.RandomSingleton;

import java.util.*;

public class OffspringSelection {

    private static final int MAX_ITERATION = 30;

    private static final double SUCCESS_RATIO = 0.3;

    private static final double MAX_SELECTION_PRESSURE = 10;

    private static final double LOWER_BOUND = 0.3;

    private static final double UPPER_BOUND = 1.0;

    private Random random = RandomSingleton.getInstance();

    private MainEvaluator evaluator;

    private MainSelection selection;

    private MainCrossover crossover;

    private MainMutation mutation;

    private List<MainSolution> children;

    private Set<MainSolution> childrenSet = new HashSet<>();

    private List<MainSolution> pool = new ArrayList<>();

    public OffspringSelection(MainEvaluator evaluator,
            short[] pinIndexes, int conductors) {
        this.evaluator = evaluator;
        this.selection = new MainSelection(3);
        this.crossover = new MainCrossover();
        this.mutation = new MainMutation(pinIndexes, conductors);
    }

    public List<MainSolution> generateOffspring(List<MainSolution> parents) {
        int size = parents.size();
        double comparisonFactor = LOWER_BOUND;
        double comparisonFactorStep = (UPPER_BOUND-LOWER_BOUND)/MAX_ITERATION;
        double actualSelectionPressure = 1.0;

        for (int i = 0; i < MAX_ITERATION; i ++) {
            if (actualSelectionPressure > MAX_SELECTION_PRESSURE) break;
            comparisonFactor += comparisonFactorStep;

            children = new ArrayList<>();
            childrenSet.clear();
            parents.stream().min(MainSolution::compareTo)
                    .ifPresent(this::addChild); // Elitism
            pool.clear();

            while (children.size() < size*SUCCESS_RATIO
                    && (children.size()+pool.size() < size*MAX_SELECTION_PRESSURE)) {
                MainSolution first = selection.select(parents);
                MainSolution second;
                do {
                    second = selection.select(parents);
                } while (first == second);
                double firstFitness = fitness(first);
                double secondFitness = fitness(second);

                MainSolution child = crossover.cross(first, second);
                int times = 2+random.nextInt(5);
                for (int j = 0; j < times; j ++) mutation.mutate(child);

                evaluator.evaluate(child);
                double childFitness = fitness(child);

                if (childFitness < secondFitness +
                        (firstFitness-secondFitness)*comparisonFactor) {
                    pool.add(child);
                } else {
                    addChild(child);
                }
            }

            actualSelectionPressure = (double) (children.size() + pool.size()) / size;

            while (children.size() < size) {
                if (pool.size() != 0) {
                    children.add(pool.get(random.nextInt(pool.size())));
                } else {
                    MainSolution first = parents.get(random.nextInt(parents.size()));
                    MainSolution second = parents.get(random.nextInt(parents.size()));

                    MainSolution child = crossover.cross(first, second);
                    evaluator.evaluate(child);
                    children.add(child);
                }
            }
            parents = children;
        }
        return parents;
    }

    private double fitness(MainSolution solution) {
        return -solution.getCost();
    }

    private void addChild(MainSolution child) {
        if (!childrenSet.contains(child)) {
            childrenSet.add(child);
            children.add(child);
        }
    }
}
