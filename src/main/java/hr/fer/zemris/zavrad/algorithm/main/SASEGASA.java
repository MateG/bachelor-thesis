package hr.fer.zemris.zavrad.algorithm.main;

import hr.fer.zemris.zavrad.algorithm.Algorithm;
import hr.fer.zemris.zavrad.algorithm.solution.Dependency;
import hr.fer.zemris.zavrad.algorithm.initial.StartingPointSolution;
import hr.fer.zemris.zavrad.algorithm.translator.SignalTranslator;
import hr.fer.zemris.zavrad.problem.Problem;

import java.util.*;

public class SASEGASA implements Algorithm<MainSolution> {

    private int populationSize;

    private int subPopulationCount;

    private int subPopulationSize;

    private MainEvaluator evaluator;

    private MainSolutionGenerator generator;

    private OffspringSelection offspringSelection;

    private Map<Dependency, Integer> unsatisfiedWeights;

    public SASEGASA(Problem problem, StartingPointSolution starting,
            int populationSize, int subPopulationCount,
            Map<Dependency, Integer> unsatisfiedWeights) {
        generator = new MainSolutionGenerator(problem, starting.getIoPinIDs());
        evaluator = new MainEvaluator(problem, starting);
        offspringSelection = new OffspringSelection(evaluator,
                pinIndexes(starting), problem.getConductorsPerSegment());
        this.populationSize = populationSize;
        this.subPopulationCount = subPopulationCount;
        subPopulationSize = populationSize / subPopulationCount;
        this.unsatisfiedWeights = unsatisfiedWeights;
    }

    public MainSolution solve() {
        List<List<MainSolution>> populations = generateSubPopulations();

        while (true) {
            printPopulations(populations);
            List<List<MainSolution>> generated = new ArrayList<>();
            for (int i = 0; i < populations.size(); i ++) {
                List<MainSolution> offspring = offspringSelection
                        .generateOffspring(populations.get(i));
                Optional<MainSolution> optional = offspring.stream()
                        .filter(MainSolution::isAcceptable).findFirst();
                if (optional.isPresent()) return optional.get();
                generated.add(offspring);
            }
            populations = generated;

            if (subPopulationCount == 1) break;

            subPopulationCount --;
            subPopulationSize = populationSize / subPopulationCount;
            joinSubPopulations(populations);
        }

        List<MainSolution> finalSolutions = populations.get(0);
        Collections.sort(finalSolutions);

        MainSolution best = finalSolutions.get(0);
        evaluator.evaluate(best);
        if (best.isAcceptable()) return best;

        evaluator.updateUnsatisfiedWeights(best, unsatisfiedWeights);
        return null;
    }

    public MainEvaluator getEvaluator() {
        return evaluator;
    }

    private List<List<MainSolution>> generateSubPopulations() {
        List<List<MainSolution>> populations = new ArrayList<>(subPopulationCount);

        int currentSize = 0;
        for (int i = 0; i < subPopulationCount; i ++) {
            int currentSubSize = i != subPopulationCount - 1 ?
                    subPopulationSize : populationSize - currentSize;

            List<MainSolution> subPopulation = new ArrayList<>(currentSubSize);
            for (int j = 0; j < currentSubSize; j ++) {
                subPopulation.add(generator.randomSolution());
            }
            for (MainSolution solution : subPopulation) {
                evaluator.evaluate(solution);
            }
            populations.add(subPopulation);

            currentSize += currentSubSize;
        }
        return populations;
    }

    private void joinSubPopulations(List<List<MainSolution>> populations) {
        List<MainSolution> allSolutions = new ArrayList<>(populationSize);
        for (int i = 0; i < subPopulationCount+1; i ++) {
            allSolutions.addAll(populations.get(i));
        }
        populations.clear();

        int currentSize = 0;
        for (int i = 0; i < subPopulationCount; i ++) {
            int currentSubSize = i != subPopulationCount - 1 ?
                    subPopulationSize : populationSize - currentSize;

            List<MainSolution> subSolutions = new ArrayList<>(currentSubSize);
            for (int j = 0; j < currentSubSize; j ++) {
                subSolutions.add(allSolutions.get(currentSize + j));
            }
            populations.add(subSolutions);

            currentSize += currentSubSize;
        }
    }

    private void printPopulations(List<List<MainSolution>> populations) {
        int size = populations.size();
        for (int i = 0; i < size - 1; i ++) {
            populations.get(i).stream().mapToInt(MainSolution::getCost).min()
                    .ifPresent(c -> System.out.print(c + " "));
        }
        populations.get(size - 1).stream().mapToInt(MainSolution::getCost).min()
                .ifPresent(System.out::println);
    }

    private short[] pinIndexes(StartingPointSolution starting) {
        short[] ioPinIDs = starting.getIoPinIDs();
        SignalTranslator translator = SignalTranslator.getInstance();
        List<Short> indexList = new ArrayList<>();
        for (short i = 0; i < ioPinIDs.length; i ++) {
            if (translator.isInputSignal(ioPinIDs[i])) indexList.add(i);
        }

        short[] pinIndexes = new short[indexList.size()];
        for (int i = 0; i < pinIndexes.length; i ++) {
            pinIndexes[i] = indexList.get(i);
        }
        return pinIndexes;
    }
}
