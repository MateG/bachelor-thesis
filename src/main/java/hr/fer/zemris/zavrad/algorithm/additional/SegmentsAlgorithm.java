package hr.fer.zemris.zavrad.algorithm.additional;

import hr.fer.zemris.zavrad.algorithm.Algorithm;
import hr.fer.zemris.zavrad.algorithm.fpga.Conductor;
import hr.fer.zemris.zavrad.algorithm.fpga.FPGA;
import hr.fer.zemris.zavrad.algorithm.fpga.Segment;
import hr.fer.zemris.zavrad.algorithm.fpga.VerticalSegment;
import hr.fer.zemris.zavrad.algorithm.search.DFS;
import hr.fer.zemris.zavrad.algorithm.solution.Evaluator;
import hr.fer.zemris.zavrad.algorithm.solution.Solution;
import hr.fer.zemris.zavrad.algorithm.solution.SolutionCleaner;
import hr.fer.zemris.zavrad.algorithm.solution.SolutionGenerator;
import hr.fer.zemris.zavrad.problem.Problem;
import hr.fer.zemris.zavrad.random.RandomSingleton;

import java.util.List;
import java.util.Random;

import static hr.fer.zemris.zavrad.algorithm.solution.SwitchBoxTranslator.*;
import static hr.fer.zemris.zavrad.algorithm.translator.SignalTranslator.NO_SIGNAL;

public class SegmentsAlgorithm implements Algorithm<Solution> {

    private Random random = RandomSingleton.getInstance();

    private Solution solution;

    private Evaluator evaluator;

    private SolutionGenerator generator;

    private int conductors;

    public SegmentsAlgorithm(Problem problem, Solution solution) {
        this.solution = solution;
        this.evaluator = new Evaluator(problem);
        this.generator = new SolutionGenerator(problem);
        this.conductors = problem.getConductorsPerSegment();
    }

    @Override
    public Solution solve() {
        evaluator.evaluate(solution);
        FPGA fpga = evaluator.getFpga();
        new SolutionCleaner(solution, fpga, conductors).clean();
        List<Conductor> usedConductors = new DFS().usedConductors(fpga);

        Solution next = generator.emptySolution();
        for (int i = 0; i < 100_000; i ++) {
            solution.overwrite(next);
            evaluator.evaluate(next);

            int conductorIndex = random.nextInt(usedConductors.size());
            Conductor conductor = usedConductors.get(conductorIndex);
            ripOutConductor(conductor);

            evaluator.evaluate(next);
            if (next.isAcceptable()) {
                System.out.println(next.getConductorsUsed());
                usedConductors.remove(conductorIndex);
                Solution solutionCopy = solution;
                solution = next;
                next = solutionCopy;
            }
        }
        return solution;
    }

    private void ripOutConductor(Conductor conductor) {
        conductor.setSignal(NO_SIGNAL);
        Segment parent = conductor.getParent();
        int index = conductor.getIndex();
        if (parent instanceof VerticalSegment) {
            int real = conductors - 1 - index;
            disconnect(parent.getFrom().getMatrix(), real, LOWER_DISCONNECTS);
            disconnect(parent.getTo().getMatrix(), real, UPPER_DISCONNECTS);
        } else {
            disconnect(parent.getFrom().getMatrix(), index, RIGHT_DISCONNECTS);
            disconnect(parent.getTo().getMatrix(), index, LEFT_DISCONNECTS);
        }
    }

    private void disconnect(byte[] matrix, int index, byte[] lookup) {
        matrix[index] = lookup[matrix[index]];
    }
}
