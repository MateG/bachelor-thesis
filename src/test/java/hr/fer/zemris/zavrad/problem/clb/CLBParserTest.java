package hr.fer.zemris.zavrad.problem.clb;

import hr.fer.zemris.zavrad.problem.architecture.Architecture;
import hr.fer.zemris.zavrad.problem.architecture.ReadOnlyArchitecture;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

/**
 * Tests the {@link CLBParser}.
 *
 * @author Mate Gašparini
 */
public class CLBParserTest {

    private static final String SINGLE = "/clb/single.txt";

    private static final String DOUBLE = "/clb/double.txt";

    private static final String COMPLEX = "/clb/complex.txt";

    private Architecture architecture = new ReadOnlyArchitecture(3, 2, 3, 3, 2);

    @Test
    public void testSingleFromString() {
        List<CLB> blocks = new CLBParser(
                "CLB(1)\nA\nB\nC", architecture
        ).getBlocks();
        Assert.assertEquals(1, blocks.size());

        CLB expected = new CLB("CLB(1)", new String[] {"A", "B", "C"}, null);
        Assert.assertEquals(expected, blocks.get(0));
    }

    @Test
    public void testSingleFromFile() throws URISyntaxException, IOException {
        List<CLB> blocks = new CLBParser(
                Paths.get(getClass().getResource(SINGLE).toURI()), architecture
        ).getBlocks();
        Assert.assertEquals(1, blocks.size());

        CLB expected = new CLB("CLB(1)", new String[] {"A", "B", "C"}, null);
        Assert.assertEquals(expected, blocks.get(0));
    }

    @Test
    public void testDoubleFromString() {
        List<CLB> blocks = new CLBParser(
                "CLB(1)\nA\nB\nC\n\nCLB(2)\nC\nB\nCLB(1)", architecture
        ).getBlocks();
        Assert.assertEquals(2, blocks.size());

        CLB clb1 = new CLB(
                "CLB(1)", new String[] {"A", "B", "C"}, null);
        Assert.assertEquals(clb1, blocks.get(0));

        CLB clb2 = new CLB(
                "CLB(2)", new String[] {"C", "B", "CLB(1)"}, null);
        Assert.assertEquals(clb2, blocks.get(1));
    }

    @Test
    public void testDoubleFromFile() throws URISyntaxException, IOException {
        List<CLB> blocks = new CLBParser(
                Paths.get(getClass().getResource(DOUBLE).toURI()), architecture
        ).getBlocks();
        Assert.assertEquals(2, blocks.size());

        CLB clb1 = new CLB(
                "CLB(1)", new String[] {"A", "B", "C"}, null);
        Assert.assertEquals(clb1, blocks.get(0));

        CLB clb2 = new CLB(
                "CLB(2)", new String[] {"C", "B", "CLB(1)"}, null);
        Assert.assertEquals(clb2, blocks.get(1));
    }

    @Test
    public void testComplexFromFile() throws URISyntaxException, IOException {
        Architecture architecture = new ReadOnlyArchitecture(3, 2, 2, 3, 2);
        CLBParser parser = new CLBParser(
                Paths.get(getClass().getResource(COMPLEX).toURI()), architecture
        );
        List<CLB> blocks = parser.getBlocks();
        Map<String, String> pinMap = parser.getPinMap();

        CLB clb1 = new CLB("CLB(1)", new String[] {"A", "B"}, null);
        Assert.assertEquals(clb1, blocks.get(0));

        CLB clb2 = new CLB("CLB(2)", new String[] {"A", "D"}, null);
        Assert.assertEquals(clb2, blocks.get(1));

        CLB clb3 = new CLB("CLB(3)", new String[] {"CLB(1)", "CLB(2)"}, "F1");
        Assert.assertEquals(clb3, blocks.get(2));

        CLB clb4 = new CLB("CLB(4)", new String[] {"CLB(3)", "C"}, "F2");
        Assert.assertEquals(clb4, blocks.get(3));

        Assert.assertEquals("1", pinMap.get("A"));
        Assert.assertEquals("2", pinMap.get("B"));
    }
}
