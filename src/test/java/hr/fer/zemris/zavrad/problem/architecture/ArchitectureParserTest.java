package hr.fer.zemris.zavrad.problem.architecture;

import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Tests the {@link ArchitectureParser}.
 *
 * @author Mate Gašparini
 */
public class ArchitectureParserTest {

    private static final String SIMPLE = "/architecture/simple.txt";

    @Test
    public void testSimpleFromString() {
        Architecture architecture = new ArchitectureParser("3\n2\n4\n5\n1\n")
                .getArchitecture();
        assertSimpleArchitecture(architecture);
    }

    @Test
    public void testSimpleFromFile() throws URISyntaxException, IOException {
        Path path = Paths.get(getClass().getResource(SIMPLE).toURI());
        Architecture architecture = new ArchitectureParser(path)
                .getArchitecture();
        assertSimpleArchitecture(architecture);
    }

    @Test
    public void testNewLinesFromString() {
        Architecture architecture = new ArchitectureParser(
                "\n3\n\n2\n4\n5\n1\n\n\n").getArchitecture();
        assertSimpleArchitecture(architecture);
    }

    @Test(expected = ArchitectureParserException.class)
    public void testMissingParameters() {
        new ArchitectureParser("1\n2\n3\n4");
    }

    @Test(expected = ArchitectureParserException.class)
    public void testExcessParameters() {
        new ArchitectureParser("1\n2\n3\n4\n5\n6");
    }

    @Test(expected = ArchitectureParserException.class)
    public void testInvalidNumberFormat() {
        new ArchitectureParser("1\n2\n3\n4\n5.0");
    }

    @Test(expected = ArchitectureParserException.class)
    public void testInvalidInput() {
        new ArchitectureParser("1\n2\n3\n4\na");
    }

    private void assertSimpleArchitecture(Architecture architecture) {
        Assert.assertEquals(3, architecture.getClbRows());
        Assert.assertEquals(2, architecture.getClbColumns());
        Assert.assertEquals(4, architecture.getClbInputs());
        Assert.assertEquals(5, architecture.getConductorsPerSegment());
        Assert.assertEquals(1, architecture.getPinsPerSegment());
    }
}
